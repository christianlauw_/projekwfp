<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignMataKuliahToDosenMataKuliah extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dosens_matakuliahs',function(Blueprint $table)
        {
            $table->integer('matakuliah_id')->unsigned();
            $table->foreign('matakuliah_id')->references('id')->on('mata_kuliahs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dosens_matakuliahs',function(Blueprint $table)
        {
            $table->dropForeign('dosens_matakuliahs_matakuliah_id_foreign');
            $table->dropColumn('matakuliah_id');
        });
    }
}
