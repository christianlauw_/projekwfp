<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignJadwalToJadwalRuanganMataKuliah extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('jadwals_ruangans_matakuliahs',function(Blueprint $table)
        {
            $table->integer('jadwalkuliah_id')->unsigned();
            $table->foreign('jadwalkuliah_id')->references('id')->on('jadwal_kuliahs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('jadwals_ruangans_matakuliahs',function(Blueprint $table)
        {
            $table->dropForeign('jadwals_ruangans_matakuliahs_jadwalkuliah_id_foreign');
            $table->dropColumn('jadwalkuliah_id');
        });
    }
}
