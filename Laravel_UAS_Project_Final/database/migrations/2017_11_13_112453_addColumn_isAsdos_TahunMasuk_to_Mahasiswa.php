<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnIsAsdosTahunMasukToMahasiswa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mahasiswas',function(Blueprint $table)
        {
            $table->boolean('isAsdos')->default(0);
            $table->integer('tahunMasuk');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mahasiswas',function(Blueprint $table)
        {
            $table->dropColumn('isAsdos');
            $table->dropColumn('tahunMasuk');
        });
    }
}
