<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnKPToMahasiswaSesiMataKuliah extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mahasiswas_sesis_matakuliahs',function(Blueprint $table)
        {
            $table->string('KP');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mahasiswas_sesis_matakuliahs',function(Blueprint $table)
        {
            $table->dropColumn('KP');
        });
    }
}
