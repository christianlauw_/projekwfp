<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignDosenToDosenMataKuliah extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dosens_matakuliahs',function(Blueprint $table)
        {
            $table->integer('dosen_id')->unsigned();
            $table->foreign('dosen_id')->references('id')->on('dosens');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dosens_matakuliahs',function(Blueprint $table)
        {
            $table->dropForeign('dosens_matakuliahs_dosen_id_foreign');
            $table->dropColumn('dosen_id');
        });
    }
}
