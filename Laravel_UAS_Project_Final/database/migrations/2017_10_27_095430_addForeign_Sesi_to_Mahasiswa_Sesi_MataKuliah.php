<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignSesiToMahasiswaSesiMataKuliah extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mahasiswas_sesis_matakuliahs',function(Blueprint $table)
        {
            $table->integer('sesi_id')->unsigned();
            $table->foreign('sesi_id')->references('id')->on('sesis');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mahasiswas_sesis_matakuliahs',function(Blueprint $table)
        {
            $table->dropForeign('mahasiswas_sesis_matakuliahs_sesi_id_foreign');
            $table->dropColumn('sesi_id');
        });
    }
}
