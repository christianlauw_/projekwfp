<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignPrioritasToMahasiswaPrioritas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mahasiswas_prioritas',function(Blueprint $table)
        {
            $table->integer('prioritas_id')->unsigned();
            $table->foreign('prioritas_id')->references('id')->on('prioritas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mahasiswas_prioritas',function(Blueprint $table)
        {
            $table->dropForeign('mahasiswas_prioritas_prioritas_id_foreign');
            $table->dropColumn('prioritas_id');
        });
    }
}
