<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignRuanganToJadwalRuanganMataKuliah extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('jadwals_ruangans_matakuliahs',function(Blueprint $table)
        {
            $table->integer('ruangan_id')->unsigned();
            $table->foreign('ruangan_id')->references('id')->on('ruangans');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('jadwals_ruangans_matakuliahs',function(Blueprint $table)
        {
            $table->dropForeign('jadwals_ruangans_matakuliahs_ruangan_id_foreign');
            $table->dropColumn('ruangan_id');
        });
    }
}
