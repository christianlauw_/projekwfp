<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignMahasiswaToMahasiswaSesiMataKuliah extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mahasiswas_sesis_matakuliahs',function(Blueprint $table)
        {
            $table->integer('mahasiswa_id')->unsigned();
            $table->foreign('mahasiswa_id')->references('id')->on('mahasiswas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mahasiswas_sesis_matakuliahs',function(Blueprint $table)
        {
            $table->dropForeign('mahasiswas_sesis_matakuliahs_mahasiswa_id_foreign');
            $table->dropColumn('mahasiswa_id');
        });
    }
}
