<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignMataKuliahToJurusanMatakuliah extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('jurusans_matakuliahs',function(Blueprint $table)
        {
            $table->integer('matakuliah_id')->unsigned();
            $table->foreign('matakuliah_id')->references('id')->on('mata_kuliahs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('jurusans_matakuliahs',function(Blueprint $table)
        {
            $table->dropForeign('jurusans_matakuliahs_matakuliah_id_foreign');
            $table->dropColumn('matakuliah_id');
        });
    }
}
