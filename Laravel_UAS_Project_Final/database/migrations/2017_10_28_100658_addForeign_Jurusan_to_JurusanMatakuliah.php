<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignJurusanToJurusanMatakuliah extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('jurusans_matakuliahs',function(Blueprint $table)
        {
            $table->integer('jurusan_id')->unsigned();
            $table->foreign('jurusan_id')->references('id')->on('jurusans');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('jurusans_matakuliahs',function(Blueprint $table)
        {
            $table->dropForeign('jurusans_matakuliahs_jurusan_id_foreign');
            $table->dropColumn('jurusan_id');
        });
    }
}
