<?php

use Illuminate\Database\Seeder;
use App\Dosen;
use App\MataKuliah;
class DosenMataKuliahTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$dos1 = Dosen::find(1);
    	$dos2 = Dosen::find(2);
    	$dos3 = Dosen::find(3);

    	$dos1->matakuliahs()->attach(5);
    	$dos1->matakuliahs()->attach(6);
    	$dos1->matakuliahs()->attach(7);

     //    $dosens_matakuliahs = array(
    	// 	['id' => 1,
    	// 	'dosen_id' => 1,
    	// 	'matakuliah_id' => 5,
    	// 		],
    	// 	['id' => 2,
    	// 	'dosen_id' => 2,
    	// 	'matakuliah_id' => 6,
    	// 		],
    	// 	['id' => 3,
    	// 	'dosen_id' => 3,
    	// 	'matakuliah_id' => 7,
    	// 		],
    	// );

    	// DB::table('dosens_matakuliahs')->insert($dosens_matakuliahs);
    }
}
