<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(AdminTableSeeder::class);
        $this->call(DosenTableSeeder::class);

        $this->call(JurusanTableSeeder::class);
        $this->call(MahasiswaTableSeeder::class);
        $this->call(PrioritasTableSeeder::class);

        $this->call(SesiTableSeeder::class);
        $this->call(MataKuliahTableSeeder::class);
        $this->call(RuanganTableSeeder::class);

        $this->call(JadwalTableSeeder::class);

        $this->call(DosenMataKuliahTableSeeder::class);
        $this->call(MahasiswaPrioritasTableSeeder::class);
         $this->call(MahasiswaSesiMataKuliahTableSeeder::class);

        $this->call(MataKuliahJurusanTableSeeder::class);
        $this->call(MataKuliahJadwalRuanganTableSeeder::class);
       $this->call(DayTableSeeder::class);
    }
}
