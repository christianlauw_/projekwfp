<?php

use Illuminate\Database\Seeder;
use App\MataKuliah;
use App\Jurusan;
class MataKuliahJurusanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $matKul1 = MataKuliah::find(1);
        $matKul2 = MataKuliah::find(2);
        $matKul3 = MataKuliah::find(3);

        $matKul1->jurusans()->attach(1);
        $matKul2->jurusans()->attach(1);
        $matKul3->jurusans()->attach(1);
    }
}
