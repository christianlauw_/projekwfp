<?php

use Illuminate\Database\Seeder;
use App\Mahasiswa;
use App\Sesi;
use App\MataKuliah;

class MahasiswaSesiMataKuliahTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	// $mhs1 = Mahasiswa::find(1);
    	// $mhs2 = Mahasiswa::find(2);
    	// $mhs3 = Mahasiswa::find(3);

    	// $sesi = new Sesi;

    	// $mhs1->sesis()->attach(1,['mataKuliah_id' => 2,'status_input'=>'0','KP'=>'A']);
    	// $mhs1->sesis()->attach(1,['mataKuliah_id' => 3,'status_input'=>'0','KP'=>'A']);
    	// $mhs1->sesis()->attach(1,['mataKuliah_id' => 4,'status_input'=>'0','KP'=>'A']);

        $sesi = new Sesi;
        for($i=1;$i<=15;$i++)
        {
            $mhs = Mahasiswa::find($i);
            $mhs->sesis()->attach(1,['mataKuliah_id' => 1,'status_input'=>'0','KP'=>'A']);
        }

     //    $mahasiswas_sesis_matakuliahs = array(
    	// 	['id' => 1,
     //        'status_input' => '0' ,
    	// 	'mahasiswa_id' => 1 ,
    	// 	'sesi_id' => 1 ,
    	// 	'matakuliah_id' => 2, 
    	// 		],
    	// 	['id' => 2,
     //        'status_input' => '0' ,
    	// 	'mahasiswa_id' => 2 ,
    	// 	'sesi_id' => 1,
    	// 	'matakuliah_id' => 3, 
    	// 		],
    	// 	['id' => 3,
     //        'status_input' => '0' ,
    	// 	'mahasiswa_id' => 3 ,
    	// 	'sesi_id' => 1,
    	// 	'matakuliah_id' => 4, 
    	// 		],
    	// );

    	// DB::table('mahasiswas_sesis_matakuliahs')->insert($mahasiswas_sesis_matakuliahs);
    }
}
