<?php

use Illuminate\Database\Seeder;
use App\Mahasiswa;
use App\User;
class MahasiswaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	// $mhs1 = new Mahasiswa([
    	// 	'nrp' => 'M001',
    	// 	'nama' => 'Bobby Si Mahasiswa',
    	// 	'ipk' => 3.00,
    	// 	'ips' => 2.87,
     //        'sks' => 24,
    	// 	'user_id' => 1,
     //        'isAsdos' => 0,
     //        'tahunMasuk'=>'2015',
     //        'jurusan_id' => 1
    	// 	]);
    	// $mhs1->save();

    	// $mhs2 = new Mahasiswa([
    	// 	'nrp' => 'M002',
     //        'nama' => 'Sonny Si Mahasiswa',
     //        'ipk' => 4.00,
     //        'ips' => 4.00,
     //        'sks' => 24,
     //        'user_id' => 2,
     //        'tahunMasuk'=>'2015',
     //        'jurusan_id' => 2
    	// 	]);
    	// $mhs2->save();

    	// $mhs3 = new Mahasiswa([
    	// 	'nrp' => 'M003',
     //        'nama' => 'Leo Si Mahasiswa',
     //        'ipk' => 2.00,
     //        'ips' => 1.90,
     //        'sks' => 20,
     //        'user_id' => 3,
     //        'tahunMasuk'=>'2015',
     //        'jurusan_id' => 3
    	// 	]);
    	// $mhs3->save();

        $nrpNext='160415123';
        for($i=1;$i<=15;$i++)
        {
            $nrpNext+=1;
            $email = $nrpNext.'@mahasiswa.ac.id';
            $user = new User([
                'username'=>$nrpNext,
                'password'=>bcrypt('12345'),
                'email'=>$email,
                'role_id'=>3
                ]);
            $user->save();

            $mhs = new Mahasiswa([
                'nrp'=>$nrpNext,
                'nama'=>'Mhs_'.$nrpNext,
                'ipk'=>3.88,
                'ips'=>3.88,
                'sks'=>24,
                'isAsdos'=>rand(0,1),
                'tahunMasuk'=>rand(2012,2015),
                'user_id'=>$user->id,
                'jurusan_id'=>1,
                ]);
            $mhs->save();
        }
     //    $mahasiswas = array(
    	// 	['id' => 1,
    	// 	'nrp' => 'M001',
    	// 	'nama' => 'Bobby Si Mahasiswa',
    	// 	'ipk' => 3.00,
    	// 	'ips' => 2.87,
    	// 	'user_id' => 1,
     //        'jurusan_id' => 1
    	// 		],
    	// 	['id' => 2,
     //        'nrp' => 'M002',
     //        'nama' => 'Sonny Si Mahasiswa',
     //        'ipk' => 4.00,
     //        'ips' => 4.00,
     //        'user_id' => 2,
     //        'jurusan_id' => 2
     //            ],
     //        ['id' => 3,
     //        'nrp' => 'M003',
     //        'nama' => 'Leo Si Mahasiswa',
     //        'ipk' => 2.00,
     //        'ips' => 1.90,
     //        'user_id' => 3,
     //        'jurusan_id' => 3
     //            ],
    	// );

    	// DB::table('mahasiswas')->insert($mahasiswas);
    }
}
