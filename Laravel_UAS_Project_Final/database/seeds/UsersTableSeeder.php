<?php

use Illuminate\Database\Seeder;
use App\User;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$u1 = new User([
    		'username' => 'mahasiswa1', 
            'email' => 'satu@mahasiswa.ac.id',
            'password' => bcrypt('mahasiswa1'),
            'role_id' => 3
    		]);
    	$u1->save();

    	$u2 = new User([
    		'username' => 'mahasiswa2', 
    		'email' => 'dua@mahasiswa.ac.id',
    		'password' => bcrypt('mahasiswa2'),
    		'role_id' => 3
    		]);
    	$u2->save();

    	$u3 = new User([
    		'username' => 'mahasiswa3', 
            'email' => 'tiga@mahasiswa.ac.id',
            'password' => bcrypt('mahasiswa3'),
            'role_id' => 3
    		]);
    	$u3->save();

    	$u4 = new User([
    		'username' => 'dosen1', 
    		'email' => 'satu@dosen.ac.id',
    		'password' => bcrypt('dosen1'),
    		'role_id' => 2
    		]);
    	$u4->save();

    	$u5 = new User([
    		'username' => 'dosen2', 
    		'email' => 'dua@dosen.ac.id',
    		'password' => bcrypt('dosen2'),
    		'role_id' => 2
    		]);
    	$u5->save();

    	$u6 = new User([
    		'username' => 'dosen3', 
            'email' => 'tiga@dosen.ac.id',
            'password' => bcrypt('dosen3'),
            'role_id' => 2
    		]);
    	$u6->save();

    	$u7 = new User([
    		'username' => 'admin1', 
    		'email' => 'satu@admin.ac.id',
    		'password' => bcrypt('admin1'),
    		'role_id' => 1
    		]);
    	$u7->save();

    	$u8 = new User([
    		'username' => 'admin2', 
            'email' => 'dua@admin.ac.id',
            'password' => bcrypt('admin2'),
            'role_id' => 1
    		]);
    	$u8->save();

    	$u9 = new User([
    		'username' => 'admin3', 
            'email' => 'tiga@admin.ac.id',
            'password' => bcrypt('admin3'),
            'role_id' => 1
    		]);
    	$u9->save();


     //    $users = array(
    	// 	['id' => 1,
     //        'username' => 'mahasiswa1', 
     //        'email' => 'satu@mahasiswa.ac.id',
     //        'password' => bcrypt('mahasiswa1'),
     //        'role_id' => 3,
     //            ],
    	// 	['id' => 2,
    	// 	'username' => 'mahasiswa2', 
    	// 	'email' => 'dua@mahasiswa.ac.id',
    	// 	'password' => bcrypt('mahasiswa2'),
    	// 	'role_id' => 3,
    	// 		],
     //        ['id' => 3,
     //        'username' => 'mahasiswa3', 
     //        'email' => 'tiga@mahasiswa.ac.id',
     //        'password' => bcrypt('mahasiswa3'),
     //        'role_id' => 3,
     //            ],
    	// 	['id' => 4,
    	// 	'username' => 'dosen1', 
    	// 	'email' => 'satu@dosen.ac.id',
    	// 	'password' => bcrypt('dosen1'),
    	// 	'role_id' => 2,
    	// 		],
    	// 	['id' => 5,
    	// 	'username' => 'dosen2', 
    	// 	'email' => 'dua@dosen.ac.id',
    	// 	'password' => bcrypt('dosen2'),
    	// 	'role_id' => 2,
    	// 		],
     //        ['id' => 6,
     //        'username' => 'dosen3', 
     //        'email' => 'tiga@dosen.ac.id',
     //        'password' => bcrypt('dosen3'),
     //        'role_id' => 2,
     //            ],
    	// 	['id' => 7,
    	// 	'username' => 'admin1', 
    	// 	'email' => 'satu@admin.ac.id',
    	// 	'password' => bcrypt('admin1'),
    	// 	'role_id' => 1,
    	// 		],
    	// 	['id' => 8,
     //        'username' => 'admin2', 
     //        'email' => 'dua@admin.ac.id',
     //        'password' => bcrypt('admin2'),
     //        'role_id' => 1,
    	// 		],
     //        ['id' => 9,
     //        'username' => 'admin3', 
     //        'email' => 'tiga@admin.ac.id',
     //        'password' => bcrypt('admin3'),
     //        'role_id' => 1,
    	// );

    	// DB::table('users')->insert($users);
    }
}
