<?php

use Illuminate\Database\Seeder;
use App\Day;

class DayTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $day1 = new Day([
        	'hari'=>'Senin'
        	]);
        $day1->save();

        $day2 = new Day([
        	'hari'=>'Selasa'
        	]);
        $day2->save();

        $day3 = new Day([
        	'hari'=>'Rabu'
        	]);
        $day3->save();

        $day4 = new Day([
        	'hari'=>'Kamis'
        	]);
        $day4->save();

        $day5 = new Day([
        	'hari'=>'Jumat'
        	]);
        $day5->save();

        $day6 = new Day([
        	'hari'=>'Sabtu'
        	]);
        $day6->save();

        $day7 = new Day([
        	'hari'=>'Minggu'
        	]);
        $day7->save();
    }
}
