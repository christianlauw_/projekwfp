<?php

use Illuminate\Database\Seeder;
use App\Mahasiswa;
use App\Prioritas;

class MahasiswaPrioritasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$mhs1 = Mahasiswa::find(1);
    	$mhs2 = Mahasiswa::find(2);
    	$mhs3 = Mahasiswa::find(3);

    	$mhs1->prioritas()->attach(2);
    	$mhs2->prioritas()->attach(1);
    	$mhs3->prioritas()->attach(3);
     //    $mahasiswas_prioritas = array(
    	// 	['id' => 1,
    	// 	'mahasiswa_id' => 1 ,
    	// 	'prioritas_id' => 2,
    	// 		],
    	// 	['id' => 2,
    	// 	'mahasiswa_id' => 2 ,
    	// 	'prioritas_id' => 1 ,
    	// 		],
    	// 	['id' => 3,
    	// 	'mahasiswa_id' => 3 ,
    	// 	'prioritas_id' => 3 ,
    	// 		],
    	// );

    	// DB::table('mahasiswas_prioritas')->insert($mahasiswas_prioritas);
    }
}
