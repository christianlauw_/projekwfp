<?php

use Illuminate\Database\Seeder;
use App\Admin;
class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ad1 = new Admin([
    		'nama' => 'Jono Si Admin', 
    		'npk' => 'A001',
    		'user_id' => 7
    		]);
    	$ad1->save();

    	$ad2 = new Admin([
    		'nama' => 'Tejo Si Admin', 
    		'npk' => 'A002',
    		'user_id' => 8
    		]);
    	$ad2->save();

    	$ad3 = new Admin([
    		'nama' => 'Bejo Si Admin', 
            'npk' => 'A003',
            'user_id' => 9
    		]);
    	$ad3->save();

    	// $admins = array(
    	// 	['id' => 1,
    	// 	'nama' => 'Jono Si Admin', 
    	// 	'npk' => 'A001',
    	// 	'users_id' => 7,
    	// 		],
    	// 	['id' => 2,
    	// 	'nama' => 'Tejo Si Admin', 
    	// 	'npk' => 'A002',
    	// 	'users_id' => 8,
    	// 		],
     //        ['id' => 3,
     //        'nama' => 'Bejo Si Admin', 
     //        'npk' => 'A003',
     //        'users_id' => 9,
     //            ],
    	// );

    	// DB::table('admins')->insert($admins);
    }
}
