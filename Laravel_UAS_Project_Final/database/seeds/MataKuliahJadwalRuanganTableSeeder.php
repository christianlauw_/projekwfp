<?php

use Illuminate\Database\Seeder;
use App\JadwalKuliah;
use App\Ruangan;
use App\MataKuliah;
class MataKuliahJadwalRuanganTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $matKul1 = MataKuliah::find(1);
        $matKul2 = MataKuliah::find(2);
        $matKul3 = MataKuliah::find(3);

        $matKul1->ruangans()->attach(1,['jadwalkuliah_id'=>1,'kp'=>'A']);
        $matKul2->ruangans()->attach(2,['jadwalkuliah_id'=>1,'kp'=>'A']);
        $matKul3->ruangans()->attach(3,['jadwalkuliah_id'=>1,'kp'=>'A']);
    }
}
