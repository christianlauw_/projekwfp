<?php

use Illuminate\Database\Seeder;
use App\Ruangan;

class RuanganTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ruang1 = new Ruangan([
        	'nama_ruangan' => 'TF 2.1',
    		'kapasitas' => 25
        	]);
        $ruang1->save();

        $ruang2 = new Ruangan([
        	'nama_ruangan' => 'TF 2.2',
    		'kapasitas' => 30
        	]);
        $ruang2->save();

        $ruang3 = new Ruangan([
        	'nama_ruangan' => 'TF 3.1',
    		'kapasitas' => 70
        	]);
        $ruang3->save();

        $ruang4 = new Ruangan([
        	'nama_ruangan' => 'TF 3.2',
    		'kapasitas' => 60
        	]);
        $ruang4->save();

        $ruang5 = new Ruangan([
        	'nama_ruangan' => 'TF 4.1',
    		'kapasitas' => 20
        	]);
        $ruang5->save();

        $ruang6 = new Ruangan([
        	'nama_ruangan' => 'TF 4.2',
    		'kapasitas' => 25
        	]);
        $ruang6->save();

     //    $ruangans = array(
    	// 	['id' => 1,
    	// 	'nama_ruangan' => 'TF 2.1',
    	// 	'kapasitas' => 25, 
    	// 		],
    	// 	['id' => 2,
    	// 	'nama_ruangan' => 'TF 2.2',
    	// 	'kapasitas' => 30, 
    	// 		],
    	// 	['id' => 3,
    	// 	'nama_ruangan' => 'TF 3.1',
    	// 	'kapasitas' => 70, 
    	// 		],
    	// 	['id' => 4,
    	// 	'nama_ruangan' => 'TF 3.2',
    	// 	'kapasitas' => 60, 
    	// 		],
    	// 	['id' => 5,
    	// 	'nama_ruangan' => 'TF 4.1',
    	// 	'kapasitas' => 20, 
    	// 		],
    	// 	['id' => 6,
    	// 	'nama_ruangan' => 'TF 4.2',
    	// 	'kapasitas' => 25, 
    	// 		],
    	// );

    	// DB::table('ruangans')->insert($ruangans);
    }
}
