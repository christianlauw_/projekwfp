<?php

use Illuminate\Database\Seeder;
use App\JadwalKuliah;

class JadwalTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $jad1 = new JadwalKuliah([
            'hari' => 'Senin',
            'jam_mulai' => '07:00:00' ,
            'jam_selesai' => '09:45:00'
            ]);
        $jad1->save();

        $jad2 = new JadwalKuliah([
            'hari' => 'Selasa',
            'jam_mulai' => '13:00:00' ,
            'jam_selesai' => '14:50:00'
            ]);
        $jad2->save();
        
        $jad3 = new JadwalKuliah([
            'hari' => 'Rabu',
            'jam_mulai' => '09:45:00' ,
            'jam_selesai' => '12:30:00'
            ]);
        $jad3->save();
        
        $jad4 = new JadwalKuliah([
            'hari' => 'Kamis',
            'jam_mulai' => '15:45:00' ,
            'jam_selesai' => '18:30:00'
            ]);
        $jad4->save();
        
        $jad5 = new JadwalKuliah([
            'hari' => 'Jumat',
            'jam_mulai' => '08:45:00' ,
            'jam_selesai' => '10:40:00'
            ]);
        $jad5->save();

        $jad6 = new JadwalKuliah([
            'hari' => 'Senin',
            'jam_mulai' => '10:40:00' ,
            'jam_selesai' => '12:30:00'
            ]);
        $jad6->save();

     //    $jadwals = array(
    	// 	['id' => 1,
    	// 	'hari' => 'Senin',
    	// 	'jam_mulai' => '07:00:00' ,
    	// 	'jam_selesai' => '09:45:00' , 
    	// 		],
    	// 	['id' => 2,
    	// 	'hari' => 'Selasa',
    	// 	'jam_mulai' => '13:00:00' ,
    	// 	'jam_selesai' => '14:50:00' , 
    	// 		],
    	// 	['id' => 3,
    	// 	'hari' => 'Rabu',
    	// 	'jam_mulai' => '09:45:00' ,
    	// 	'jam_selesai' => '12:30:00' , 
    	// 		],
    	// 	['id' => 4,
    	// 	'hari' => 'Kamis',
    	// 	'jam_mulai' => '15:45:00' ,
    	// 	'jam_selesai' => '18:30:00' , 
    	// 		],
    	// 	['id' => 5,
    	// 	'hari' => 'Jumat',
    	// 	'jam_mulai' => '08:45:00' ,
    	// 	'jam_selesai' => '10:40:00' , 
    	// 		],
    	// 	['id' => 6,
    	// 	'hari' => 'Senin',
    	// 	'jam_mulai' => '10:40:00' ,
    	// 	'jam_selesai' => '12:30:00' , 
    	// 		],
    	// );

    	// DB::table('jadwals')->insert($jadwals);
    }
}
