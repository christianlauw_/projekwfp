<?php

use Illuminate\Database\Seeder;
use App\Role;
class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $r1 = new Role([
            'jenis_role' => 'admin'
            ]);
        $r1->save();

        $r2 = new Role([
            'jenis_role' => 'dosen'
            ]);
        $r2->save();

        $r3 = new Role([
            'jenis_role' => 'mahasiswa'
            ]);
        $r3->save();

        // $roles = array(
        //     ['id' => 1,
        //     'jenis_role' => 'admin', 
        //         ],
        //     ['id' => 2,
        //     'jenis_role' => 'dosen', 
        //         ],
        //     ['id' => 3,
        //     'jenis_role' => 'mahasiswa', 
        //         ],
        // );

        // DB::table('roles')->insert($roles);
    }
}
