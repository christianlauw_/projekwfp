<?php

use Illuminate\Database\Seeder;
use App\Jurusan;

class JurusanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$jur1= new Jurusan([
    		'kode_jurusan' => 'J001' ,
    		'nama_jurusan' => 'Teknik Informatika'
    		]);
    	$jur1->save();

    	$jur2= new Jurusan([
    		'kode_jurusan' => 'J002' ,
    	 	'nama_jurusan' => 'Multimedia'
    		]);
    	$jur2->save();

    	$jur3= new Jurusan([
    		'kode_jurusan' => 'J003' ,
    	 	'nama_jurusan' => 'Sistem Informasi'
    		]);
    	$jur3->save();
        
     //    $jurusans = array(
    	// 	['id' => 1,
    	// 	'kode_jurusan' => 'J001' ,
    	// 	'nama_jurusan' => 'Teknik Informatika' ,
    	// 		],
    	// 	['id' => 2,
    	// 	'kode_jurusan' => 'J002' ,
    	// 	'nama_jurusan' => 'Multimedia' ,
    	// 		],
    	// 	['id' => 3,
    	// 	'kode_jurusan' => 'J003' ,
    	// 	'nama_jurusan' => 'Sistem Informasi' ,
    	// 		],
    	// );

    	// DB::table('jurusans')->insert($jurusans);
    }
}
