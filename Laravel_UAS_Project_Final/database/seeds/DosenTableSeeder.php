<?php

use Illuminate\Database\Seeder;
use App\Dosen;

class DosenTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$dos1 = new Dosen([
    		'nama' => 'Laura Si Dosen', 
    		'npk' => 'D001',
    		'user_id' => 4
    		]);
    	$dos1->save();

    	$dos2 = new Dosen([
    		'nama' => 'Lia Si Dosen', 
            'npk' => 'D002',
            'user_id' => 5
    		]);
    	$dos2->save();

    	$dos3 = new Dosen([
    		'nama' => 'Paul Si Dosen', 
            'npk' => 'D003',
            'user_id' => 6
    		]);
    	$dos3->save();

     //    $dosens = array(
    	// 	['id' => 1,
    	// 	'nama' => 'Laura Si Dosen', 
    	// 	'npk' => 'D001',
    	// 	'users_id' => 4,
    	// 		],
     //        ['id' => 2,
     //        'nama' => 'Lia Si Dosen', 
     //        'npk' => 'D002',
     //        'users_id' => 5,
     //            ],
     //        ['id' => 3,
     //        'nama' => 'Paul Si Dosen', 
     //        'npk' => 'D003',
     //        'users_id' => 6,
     //            ],
    	// );

    	// DB::table('dosens')->insert($dosens);
    }
}
