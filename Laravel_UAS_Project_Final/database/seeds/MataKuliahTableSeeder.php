<?php

use Illuminate\Database\Seeder;
use App\MataKuliah;
class MataKuliahTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$matkul1 = new MataKuliah([
            'kode_matkul' => 'M001',
    		'nama' => 'Web Framework Programming',
    		'sks' => 3,
            'semester'=>5,
    		'minggu_ujian' => 1,
    		'hari_ujian' => 'Senin',
    		'jam_ujian' => 2
    		]);
    	$matkul1->save();

    	$matkul2 = new MataKuliah([
            'kode_matkul' => 'M002',
    		'nama' => 'Algoritma dan Pemograman',
    		'sks' => 6,
            'semester'=>1,
    		'minggu_ujian' => 1,
    		'hari_ujian' => 'Rabu',
    		'jam_ujian' => 3
    		]);
    	$matkul2->save();

    	$matkul3 = new MataKuliah([
            'kode_matkul' => 'M003',
    		'nama' => 'Desain Web',
    		'sks' => 2,
            'semester'=>2,
    		'minggu_ujian' => 2,
    		'hari_ujian' => 'Selasa',
    		'jam_ujian' => 1
    		]);
    	$matkul3->save();

    	$matkul4 = new MataKuliah([
            'kode_matkul' => 'M004',
    		'nama' => 'Pemrograman Web',
    		'sks' => 3,
            'semester'=>4,
    		'minggu_ujian' => 1,
    		'hari_ujian' => 'Kamis',
    		'jam_ujian' => 2
    		]);
    	$matkul4->save();

    	$matkul5 = new MataKuliah([
            'kode_matkul' => 'M005',
    		'nama' => 'Matematika',
    		'sks' => 4,
            'semester'=>1,
    		'minggu_ujian' => 1,
    		'hari_ujian' => 'Kamis',
    		'jam_ujian' => 3
    		]);
    	$matkul5->save();

    	$matkul6 = new MataKuliah([
            'kode_matkul' => 'M006',
    		'nama' => 'Statistika',
    		'sks' => 4,
            'semester'=>5,
    		'minggu_ujian' => 1,
    		'hari_ujian' => 'Kamis',
    		'jam_ujian' => 2
    		]);
    	$matkul6->save();

    	$matkul7 = new MataKuliah([
            'kode_matkul' => 'M007',
    		'nama' => 'Struktur Data',
    		'sks' => 4,
            'semester'=>4,
    		'minggu_ujian' => 1,
    		'hari_ujian' => 'Jumat',
    		'jam_ujian' => 3
    		]);
    	$matkul7->save();

    	$matkul8 = new MataKuliah([
            'kode_matkul' => 'M008',
    		'nama' => 'Matematika Diskrit',
    		'sks' => 3,
            'semester'=>2,
    		'minggu_ujian' => 2,
    		'hari_ujian' => 'Rabu',
    		'jam_ujian' => 2
    		]);
    	$matkul8->save();

    	$matkul9 = new MataKuliah([
            'kode_matkul' => 'M009',
    		'nama' => 'Pengantar Informatika',
    		'sks' => 2,
            'semester'=>1,
    		'minggu_ujian' => 2,
    		'hari_ujian' => 'Selasa',
    		'jam_ujian' => 1
    		]);
    	$matkul9->save();

     //    $mata_kuliahs = array(
    	// 	['id' => 1,
    	// 	'nama' => 'Web Framework Programming',
    	// 	'sks' => 3,
    	// 	'minggu_ujian' => 1,
    	// 	'hari_ujian' => 'Senin',
    	// 	'jam_ujian' => 2,
    	// 		],
    	// 	['id' => 2,
    	// 	'nama' => 'Algoritma dan Pemograman',
    	// 	'sks' => 6,
    	// 	'minggu_ujian' => 1,
    	// 	'hari_ujian' => 'Rabu',
    	// 	'jam_ujian' => 3,
    	// 		],
    	// 	['id' => 3,
    	// 	'nama' => 'Desain Web',
    	// 	'sks' => 2,
    	// 	'minggu_ujian' => 2,
    	// 	'hari_ujian' => 'Selasa',
    	// 	'jam_ujian' => 1,
    	// 		],
    	// 	['id' => 4,
    	// 	'nama' => 'Pemrograman Web',
    	// 	'sks' => 3,
    	// 	'minggu_ujian' => 1,
    	// 	'hari_ujian' => 'Kamis',
    	// 	'jam_ujian' => 2,
    	// 		],
    	// 	['id' => 5,
    	// 	'nama' => 'Matematika',
    	// 	'sks' => 4,
    	// 	'minggu_ujian' => 1,
    	// 	'hari_ujian' => 'Kamis',
    	// 	'jam_ujian' => 3,
    	// 		],
    	// 	['id' => 6,
    	// 	'nama' => 'Statistika',
    	// 	'sks' => 4,
    	// 	'minggu_ujian' => 1,
    	// 	'hari_ujian' => 'Kamis',
    	// 	'jam_ujian' => 2,
    	// 		],
    	// 	['id' => 7,
    	// 	'nama' => 'Struktur Data',
    	// 	'sks' => 4,
    	// 	'minggu_ujian' => 1,
    	// 	'hari_ujian' => 'Jumat',
    	// 	'jam_ujian' => 3,
    	// 		],
    	// 	['id' => 8,
    	// 	'nama' => 'Matematika Diskrit',
    	// 	'sks' => 3,
    	// 	'minggu_ujian' => 2,
    	// 	'hari_ujian' => 'Rabu',
    	// 	'jam_ujian' => 2,
    	// 		],
    	// 	['id' => 9,
    	// 	'nama' => 'Pengantar Informatika',
    	// 	'sks' => 2,
    	// 	'minggu_ujian' => 2,
    	// 	'hari_ujian' => 'Selasa',
    	// 	'jam_ujian' => 1,
    	// 		],
    	// );

    	// DB::table('mata_kuliahs')->insert($mata_kuliahs);
    }
}
