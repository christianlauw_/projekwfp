<?php

use Illuminate\Database\Seeder;
use App\Prioritas;

class PrioritasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$prior1 = new Prioritas([
    		'jenis_prioritas' => 'asisten dosen'
    		]);
    	$prior1->save();

    	$prior2 = new Prioritas([
    		'jenis_prioritas' => 'mahasiswa pada semesternya'
    		]);
    	$prior2->save();

    	$prior3 = new Prioritas([
    		'jenis_prioritas' => 'mahasiswa angkatan tua'
    		]);
    	$prior3->save();

    	$prior4 = new Prioritas([
    		'jenis_prioritas' => 'lainnya'
    		]);
    	$prior4->save();

   //      $prioritas = array(
   //  		['id' => 1,
   //  		'jenis_prioritas' => 'asisten dosen'
   //  			],
   //  		['id' => 2,
   //  		'jenis_prioritas' => 'mahasiswa pada semesternya'
   //  			],
   //  		['id' => 3,
   //  		'jenis_prioritas' => 'mahasiswa angkatan tua'
   //  			],
			// ['id' => 4,
   //  		'jenis_prioritas' => 'lainnya'
   //  			],
   //  	);

   //  	DB::table('prioritas')->insert($prioritas);
    }
}
