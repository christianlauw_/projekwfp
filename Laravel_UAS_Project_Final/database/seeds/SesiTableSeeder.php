<?php

use Illuminate\Database\Seeder;
use App\Sesi;

class SesiTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$ses1= new Sesi([
    		'nama_sesi' => 'FPP1',
    		'start_input' => '2017/07/25 00:00:00',
    		'finish_input' => '2017/07/25 15:00:00'
    		]);
    	$ses1->save();

    	$ses2= new Sesi([
    		'nama_sesi' => 'FPP2',
    		'start_input' => '2017/07/27 20:00:00',
    		'finish_input' => '2017/07/28 13:00:00'
    		]);
    	$ses2->save();

    	$ses3= new Sesi([
    		'nama_sesi' => 'Kasus Khusus',
    		'start_input' => '2017/07/31 06:00:00',
    		'finish_input' => '2017/07/31 13:00:00'
    		]);
    	$ses3->save();

     //    $sesis = array(
    	// 	['id' => 1,
    	// 	'nama_sesi' => 'FPP1',
    	// 	'start_input' => '2017/07/25 00:00:00',
    	// 	'finish_input' => '2017/07/25 15:00:00', 
    	// 		],
    	// 	['id' => 2,
    	// 	'nama_sesi' => 'FPP2',
    	// 	'start_input' => '2017/07/27 20:00:00',
    	// 	'finish_input' => '2017/07/28 13:00:00', 
    	// 		],
    	// 	['id' => 3,
    	// 	'nama_sesi' => 'Kasus Khusus',
    	// 	'start_input' => '2017/07/31 06:00:00',
    	// 	'finish_input' => '2017/07/31 13:00:00', 
    	// 		],
    	// );

    	// DB::table('sesis')->insert($sesis);
    }
}
