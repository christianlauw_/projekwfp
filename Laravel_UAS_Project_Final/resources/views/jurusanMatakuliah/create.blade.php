@section('nama', 'Daftar Jurusan Matakuliah')
@extends('layouts.index')
@section('content')

<form method="post" action="{{url('jurusanMatakuliah/')}}">
	{{csrf_field()}}
	<label for="jurusan" style="width:20%">Nama Jurusan :</label>
	<select name="jurusan">
		<option>--Pilih Jurusan--</option>
		@foreach($jurusan as $item)
			<option value="{{$item->id}}">{{$item->kode_jurusan}} - {{$item->nama_jurusan}}</option>
		@endforeach
	</select><br/>

	<label for="matkul" style="width:20%">Mata Kuliah :</label>
	<select name="matkul">
		<option>--Pilih Mata Kuliah--</option>
		@foreach($matkul as $item)
			<option value="{{$item->id}}">{{$item->kode_matkul}} - {{$item->nama}}</option>
		@endforeach
	</select><br/>
<input type="submit" name="" class="btn btn-primary" value="Tambah MataKuliah">
</form>
<table class="table table-hover">
	<tr>
		<th>Jurusan</th>
		<th>Mata Kuliah</th>
		<th colspan="2">Action</th>
	</tr>
@foreach($jurusan as $item)
	<tr>
		<td>{{$item->nama_jurusan}}</td>
		<td>
			@foreach($item->matakuliahs as $mk)
				{{$mk->kode_matkul}} - {{$mk->nama}}<br>
			@endforeach
		</td>
		<td>
			@foreach($item->matakuliahs as $mk)
				<form action="{{url('jurusanMatakuliah/'.$item->id.'/'.$mk->id)}}" method="post">
						{{csrf_field()}}
			         	<input name="_method" type="hidden" value="DELETE"/>	
			         	<button type="submit" class="btn btn-xs btn-danger btn_delete">Delete</button>  	
					</form>
			@endforeach
		</td>
	</tr>
@endforeach
</table>

@endsection