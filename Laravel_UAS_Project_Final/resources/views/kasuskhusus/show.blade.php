@section('nama', 'Kasus Khusus')
@extends('layouts.index')

@section('content')
<h1>Kasus Khusus</h1>
<?php $statusOpen=0; 
	$date = new DateTime();
	$date->setTimezone(new DateTimeZone('Asia/Jakarta'));
	$time = $date->format('Y-m-d H:i:s')?> 

@foreach($sesi as $itemSesi)
	@if($itemSesi->id==3)
		@if($itemSesi->start_input<=$time AND $itemSesi->finish_input>=$time)
			@foreach($sesi as $sesis)
			<h1>{{$sesis->nama_sesi}}</h1>
			<table class="table table-hover">
				<tr>
						<th>Kode Matkul</th>
						<th>Nama Matkul</th>
						<th>KP</th>
						<th>SKS</th>
						<th>status</th>
						<th>Cancel</th>
				</tr>
				@foreach($fppMhs as $item)
					@if($item->Sesi==$sesis->id)
					<tr>
						<td>
							{{$item->kode}}
						</td>
						<td>
							{{$item->matkul}}
						</td>
						<td>
							{{$item->KP}}
						</td>
						<td>
							{{$item->sks}}
						</td>
						@if($item->statInput=='0')
							<td>Pending</td>
						@elseif($item->statInput=='0.5')
							<td>Ditolak</td>
						@elseif($item->statInput=='0.75')
							<td>Dicancel</td>
						@elseif($item->statInput=='1')	
							<td>Diterima</td>
						@endif

						@if($item->statInput=='1')
							<td>
							<a href="{{url('/kasuskhusus/'.$id.'/'.$item->matakuliah_id.'/'.$item->Sesi.'/'.$item->KP)}}">Cancel Matkul</a>
							</td>
						@endif
					</tr>
					@endif
				@endforeach
			</table>
			@endforeach
		@else
			<p>Kasus Khusus Belum dibuka</p>
		@endif
	@endif
@endforeach
@endsection