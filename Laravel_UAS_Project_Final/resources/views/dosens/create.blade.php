@section('nama', 'Register Dosen')
@extends('layouts.index')

@section('content')
<h1>Dosen Baru</h1>
@if($errors->any())
  <div class="alert alert-danger">
    <ul>
      @foreach($errors->all() as $error)
        <li>{{$error}}</li>
      @endforeach
    </ul>
  </div>
@endif
<form method="post" action="{{url('dosen')}}">
	{{csrf_field()}}
	<label for="npk" style="width:20%">NPK = Username: </label><input type="text" name="npk" id="npk"><br/>
	<label for="nama" style="width:20%">Nama : </label><input type="text" name="nama" id="nama"><br/>
	<label for="pass" style="width:20%">Password : </label><input type="password" name="pass" id="pass"><br/>
	<input type="submit" class="btn btn-primary" value="Tambah Dosen"/>
	<br>
</form>
<table class="table table-hover">
	<tr>
		<th>NPK</th>
		<th>Nama</th>
		@if(Auth::user()->isAdmin())
			<th>Edit</th>
			<th>Delete</th>
		@endif
	</tr>
	@foreach($dosen as $item)
		<tr>
			<td>{{$item->npk}}</td>
			<td>{{$item->nama}}</td>
			@if(Auth::user()->isAdmin())
				<td><a class="btn btn-xs btn-success" href="{{action('DosenController@edit', $item->id)}}" name="edit_dosen" id_dosen="{{$item->id}}">Edit</a></td>
				<td><form action="{{action('DosenController@destroy',$item->id)}}" method="post">
					{{csrf_field()}}
		         	<input name="_method" type="hidden" value="DELETE"/>	
		         	<button type="submit" class="btn btn-xs btn-danger btn_delete">Delete</button>  	
				</form></td>
			@endif
		</tr>
		<tr>
			<td></td>
			<td colspan="3">
				<div class="dosen-{{$item->id}}"></div>
			</td>
		</tr>
	@endforeach
</table>
<script type="text/javascript" src="{{asset('/js/my.js')}}"></script>
@endsection
