<!-- section('nama', 'Edit Dosen') -->
<!-- extends('layouts.index') -->

<!-- section('content') -->
<!-- <h1>Ubah Data Dosen</h1> -->
@if($errors->any())
  <div class="alert alert-danger">
    <ul>
      @foreach($errors->all() as $error)
        <li>{{$error}}</li>
      @endforeach
    </ul>
  </div>
@endif

<form method="post" action="{{action('DosenController@update', $id)}}">
	{{csrf_field()}}
	<input name="_method" type="hidden" value="PATCH">
	<label style="width: 20%;">NPK = Username: </label><input type="text" name="npk" disabled value="{{$dos->npk}}"><br/>
	<label style="width: 20%;">Nama : </label><input type="text" name="nama" value="{{$dos->nama}}" ><br/>
	<input type="submit" class="btn" value="Ubah data Dosen"/>
</form>
<!-- endsection -->
