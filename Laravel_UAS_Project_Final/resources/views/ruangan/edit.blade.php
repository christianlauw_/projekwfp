<!-- section('nama', 'Edit Ruangan') -->
<!-- extends('layouts.index') -->

<!-- section('content') -->
<!-- <h1>Ubah Data Ruangan</h1> -->
@if($errors->any())
  <div class="alert alert-danger">
    <ul>
      @foreach($errors->all() as $error)
        <li>{{$error}}</li>
      @endforeach
    </ul>
  </div>
@endif

<form method="post" action="{{action('RuanganController@update', $id)}}">
  {{csrf_field()}}
  <input name="_method" type="hidden" value="PATCH">
  <label style="width: 20%;">Nama Ruangan </label><input type="text" name="nama_ruangan" disabled value="{{$ruang->nama_ruangan}}"><br/>
  <label style="width: 20%;">Kapasitas : </label><input type="text" name="kapasitas" value="{{$ruang->kapasitas}}"><br/>
  <input type="submit" class="btn" value="Ubah data Ruangan"/>
</form>
<!-- endsection -->
