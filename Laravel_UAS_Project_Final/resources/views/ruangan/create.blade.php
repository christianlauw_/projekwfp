@section('nama', 'Register Ruangan')
@extends('layouts.index')

@section('content')
<h1>Ruangan Baru</h1>
@if($errors->any())
  <div class="alert alert-danger">
    <ul>
      @foreach($errors->all() as $error)
        <li>{{$error}}</li>
      @endforeach
    </ul>
  </div>
@endif
<form method="post" action="{{url('ruangan')}}">
	{{csrf_field()}}
	<label for="nama_ruangan" style="width:20%">Nama Ruang :</label> <input type="text" name="nama_ruangan" id="nama_ruangan"><br/>
	<label for="kapasitas" style="width:20%">Kapasitas : </label><input type="number" min="1" max="150" name="kapasitas" id="kapasitas"><br/>
	<input type="submit" class="btn btn-primary" value="Tambah Ruang"/>
</form>


	<table class="table table-hover">
	<tr>
		<th>Nama Ruang</th>
		<th>Kapasitas</th>
		@if(Auth::user()->isAdmin())
			<th>Edit</th>
			<th>Delete</th>
		@endif
	</tr>
	@foreach($ruang as $item)
		<tr>
			<td>{{$item->nama_ruangan}}</td>
			<td>{{$item->kapasitas}}</td>
			@if(Auth::user()->isAdmin())
				<td><a class="btn btn-xs btn-success" href="{{action('RuanganController@edit', $item->id)}}" name="edit_ruangan" id_ruangan="{{$item->id}}">Edit</a></td>
				<td><form action="{{action('RuanganController@destroy',$item->id)}}" method="post">
					{{csrf_field()}}
		         	<input name="_method" type="hidden" value="DELETE"/>	
		         	<button type="submit" class="btn btn-xs btn-danger btn_delete">Delete</button>  	
				</form></td>
			@endif
		</tr>
		<tr>
			<td></td>
			<td colspan="3">
				<div class="ruang-{{$item->id}}"></div>
			</td>
		</tr>
	@endforeach
	</table>
	<script type="text/javascript" src="{{asset('/js/my.js')}}"></script>

@endsection