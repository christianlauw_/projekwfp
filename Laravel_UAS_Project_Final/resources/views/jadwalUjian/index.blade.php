@section('nama', 'Jadwal Ujian')
@extends('layouts.index')

@section('pilih')

<br>
	<select name="pilihjurusan">
		@foreach($jurusan as $item)
	        <option value="{{$item->id}}">{{$item->nama_jurusan}}</option>
        @endforeach
    </select>
    <br>

@endsection

@section('content')
<!-- <a href="{{url('/')}}">Home</a> -->
<h1>Jadwal Ujian</h1>
<table class="table table-hover">

@foreach($matkulMingguUjian as $item)
	<tr >
		<th colspan="7" style="text-align:center;">Minggu ke - {{$item->mg_ujian}}</th>
	</tr>
	<tr>
		<th colspan="7" style="text-align:center;">Hari Ujian</th>
	</tr>
	<th>Jam-Ke</th>
	@foreach($days as $item2)
		<th>{{$item2->hari}}</th>
	@endforeach
	@for($i=1;$i<=4;$i++)
		<tr>
			<td>{{$i}}</td>
		@foreach($days as $item3)
			@if($item3->hari!='Minggu')
			@foreach($matkul as $item4)
				@if($item4->mgg_ujian== $item->mg_ujian AND $item4->hr_ujian==$item3->hari AND $item4->jm_ujian==$i)
					<td>{{$item4->kode_matkul}} - {{$item4->nama}}</td>
				@endif
			@endforeach
			<td>Belum ada Ujian</td>
			@endif
		@endforeach
		</tr>
	@endfor
@endforeach
</table>
@endsection