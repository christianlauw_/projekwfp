<!DOCTYPE html>
<html lang="en">

<head>
  <!-- <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
  <meta name="author" content="GeeksLabs">
  <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal"> -->
  <link rel="shortcut icon" href="img/favicon.png">

  <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>Perwalian Fakultas Teknik Surabaya</title>

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="{{ asset('css/tes/bootstrap.css') }}">
  <link rel="stylesheet" href="{{ asset('css/tes/bootstrap.min.css') }}"><link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
  <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('css/app.css') }}">
  <!-- theme css -->
  <link rel="stylesheet" href="{{ asset('css/tes/bootstrap-theme.css') }}">
  <!-- font icon -->
  <link rel="stylesheet" href="{{ asset('css/tes/elegant-icons-style.css') }}">
  <link rel="stylesheet" href="{{ asset('css/tes/font-awesome.min.css') }}">
  <!-- full calendar css-->
  <link href="  asset/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
  <link href="  asset/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" />
  <!-- easy pie chart-->
  <link href="  asset/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen" />
  <!-- owl carousel -->
  <link rel="stylesheet" href="css/owl.carousel.css" type="text/css">
  <link href="css/jquery-jvectormap-1.2.2.css" rel="stylesheet">
  <!-- Custom styles -->
  <link rel="stylesheet" href="{{ asset('css/tes/fullcalendar.css') }}">
  <link href="{{ asset('css/tes/widgets.css') }}" rel="stylesheet">
  <link href="{{ asset('css/tes/style.css') }}" rel="stylesheet">
  <link href="{{ asset('css/tes/style-responsive.css') }}" rel="stylesheet" />
  <link href="{{ asset('css/tes/xcharts.min.css') }}" rel=" stylesheet">
  <link href="{{ asset('css/tes/jquery-ui-1.10.4.min.css') }}" rel="stylesheet">

  <!-- css popup -->
  <link rel="stylesheet" href="{{ asset('css/tes/formpopup.css') }}">
  <script type="text/javascript" src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
    <script type="text/javascript" src="{{asset('js/jquery-ui.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function()
        {
            function updateTime(){
                $.get('{{url("/serverTime")}}',function(data)
                {
                    if(data.jam!=null)
                    {
                        $('#time').text(data.jam +" Timezone: "+data.timezoneUsed); 
                    }
                });
                setTimeout(updateTime,1000);
            }
            updateTime();

            $('select[name="kodematkul"]').on('change',function()
            {
                var kodematkul = $(this).val();
                
                $("#kp").empty();
                $("#kp").append("<option value=''>--Pilih KP--</option>");
                $.get('{{url("/fpp/ajax/")}}/'+kodematkul,function(data)
                {
                    if(data.kp!=null)
                    {
                        for(i=0;i<data.kp.length;i++)
                        {
                            $("#kp").append("<option value=\""+data.kp[i].KP+"\">"+data.kp[i].KP+"</option>");
                        }
                    }
                });
            });
            $("#kp").on('change',function()
            {
                var kodematkul = $('select[name="kodematkul"]').val();
                var kp = $(this).val();
                $.get('{{url("/fpp/ajax/")}}/'+kodematkul+'/'+kp,function(data)
                {
                    if(data!=null)
                    {
                        $("#kapa_KP").text(data.jumKap);
                        $("#kapa_isi_KP").text(data.isi);
                    }
                });
            });

            // $('#kk_idMhs').on('change',function()
            // {
            //     var id=$(this).val();
            //     alert(id);
            //     $("#link_kk_id").attr("href","{{url('kasuskhusus/')}}/"+id);
            // });

            $("#namaMhs").on('input', function () {
                var id=$(this).val();
                var id2 = $('#nama_mhs [value="' + id + '"]').data('customvalue');
                // alert(id2);
                $("#link_kk_id").attr("href","{{url('kasuskhusus/')}}/"+id2);
            });

            $("#search_jadwalMatkul").keyup(function(){
              var searchText = $(this).val().toLowerCase();
              // Show only matching TR, hide rest of them
              $.each($("#tabel_jadwalMatkul tbody tr"), function() {
                  if($(this).text().toLowerCase().indexOf(searchText) === -1)
                     $(this).hide();
                  else
                     $(this).show();                
              });
            });
            $("#search_mataKuliahList").keyup(function(){
              var searchText = $(this).val().toLowerCase();
              // Show only matching TR, hide rest of them
              $.each($("#tabel_mataKuliahList tbody tr"), function() {
                  if($(this).text().toLowerCase().indexOf(searchText) === -1)
                     $(this).hide();
                  else
                     $(this).show();                
              });
            });
            $("#search_fpp_1").keyup(function(){
              var searchText = $(this).val().toLowerCase();
              //alert(searchText);
              // Show only matching TR, hide rest of them
              $.each($("#tabel_fpp_1 tbody tr"), function() {
                  if($(this).text().toLowerCase().indexOf(searchText) === -1)
                     $(this).hide();
                  else
                     $(this).show();                
              });
            });
            $("#search_fpp_2").keyup(function(){
              var searchText = $(this).val().toLowerCase();
              // Show only matching TR, hide rest of them
              $.each($("#tabel_fpp_2 tbody tr"), function() {
                  if($(this).text().toLowerCase().indexOf(searchText) === -1)
                     $(this).hide();
                  else
                     $(this).show();                
              });
            });
            $("#search_fpp_3").keyup(function(){
              var searchText = $(this).val().toLowerCase();
              // Show only matching TR, hide rest of them
              $.each($("#tabel_fpp_3 tbody tr"), function() {
                  if($(this).text().toLowerCase().indexOf(searchText) === -1)
                     $(this).hide();
                  else
                     $(this).show();                
              });
            });
        });
    </script>
  <!-- =======================================================
    Theme Name: NiceAdmin
    Theme URL: https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/
    Author: BootstrapMade
    Author URL: https://bootstrapmade.com
  ======================================================= -->
</head>

<body>
  <!-- container section start -->
  <section id="container" class="">


    <header class="header dark-bg">
      <div class="toggle-nav">
        <div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom"><i class="icon_menu"></i></div>

      </div>

      <!--logo start-->

      <a href="{{url('/')}}" class="logo">Fakultas <span class="lite">Teknik</span> <span id="time"></span></a>

      <!--logo end-->

      <div class="nav search-row" id="top_menu">
        <!--  search form start -->
        <ul class="nav top-menu">
          <li>
           <!--  <form class="navbar-form">
              <input class="form-control" placeholder="Search" type="text">
            </form> -->
          </li>
        </ul>
        <!--  search form end -->
      </div>

      <div class="top-nav notification-row">
        <!-- notificatoin dropdown start-->
        <ul class="nav pull-right top-menu">

          <!-- task notificatoin start -->
          
          <!-- alert notification end-->
          <!-- user login dropdown start--> 
          <li class="dropdown">
           
<!-- if dulu kalau dia sudah login -->
@if (Route::has('login'))
  @if (Auth::check())
         <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
            @if(Auth::user()->isAdmin())
                {{ Auth::user()->admin->nama }}
            @elseif(Auth::user()->isMahasiswa())
                {{ Auth::user()->mahasiswa->nama }} - {{Auth::user()->mahasiswa->jurusan->nama_jurusan}}
            @elseif(Auth::user()->isDosen())
                {{Auth::user()->dosen->nama}}
            @endif
            <span class="caret"></span>
        </a>

          <ul class="dropdown-menu extended logout">
              <div class="log-arrow-up"></div>
              <li class="eborder-top">
                <a href="#"><i class="icon_profile"></i> My Profile</a>
              </li>
              <!-- <li>
                <a href="#"><i class="icon_mail_alt"></i> My Inbox</a>
              </li>
              <li>
                <a href="#"><i class="icon_clock_alt"></i> Timeline</a>
              </li>
              <li>
                <a href="#"><i class="icon_chat_alt"></i> Chats</a>
              </li> -->
              <li>
                <a href="{{ route('logout') }}"
                  onclick="event.preventDefault();
                           document.getElementById('logout-form').submit();"><i class="icon_key_alt"></i>
                  Logout
                </a>

                  <form id="logout-form" action="{{ route('logout') }}" method="POST" >
                      {{ csrf_field() }}
                  </form>
              </li>
              <!-- <li>
                <a href="documentation.html"><i class="icon_key_alt"></i> Documentation</a>
              </li>
              <li>
                <a href="documentation.html"><i class="icon_key_alt"></i> Documentation</a>
              </li> -->
            </ul>

              

              </div>

                                    
<!-- else kalau dia blm login --> 
        @else
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Log In <span class="caret"></span></a>
              <ul class="dropdown-menu dropdown-lr animated slideInRight" role="menu">

                  <div class="col-lg-12" style="background-color: #1a2732">
                      <div class="text-center"><h3><b>Log In</b></h3></div>
                      <form id="ajax-login-form" action="{{ route('login') }}" method="post" role="form" autocomplete="off">
                        {{ csrf_field() }}
                          <div class="form-group">
                              <label for="username">Username</label>
                              <input type="text" name="username" id="username" tabindex="1" class="form-control" placeholder="Username" value="" autocomplete="off">
                          </div>

                          <div class="form-group">
                              <label for="password">Password</label>
                              <input type="password" name="password" id="password" tabindex="2" class="form-control" placeholder="Password" autocomplete="off">
                          </div>

                          <div class="form-group">
                              <div class="row">
                                  <!-- <div class="col-xs-7">
                                      <input type="checkbox" tabindex="3" name="remember" id="remember">
                                      <label for="remember"> Remember Me</label>
                                  </div> -->
                                  <div class="col-xs-5 pull-right">
                                      <input type="submit" name="login-submit" id="login-submit" tabindex="4" class="form-control btn btn-success" value="Log In">
                                  </div>
                              </div>
                          </div>
                      </form>
                  </div>
              </ul>

          </li>
          @endif
    @endif
          <!-- user login dropdown end -->
        </ul>
      </div>
    </header>
    <!--header end-->

    <!--sidebar start-->
@if (Route::has('login'))

    <aside>
      <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu">
              <!-- INI MENU HOME -->
              <li>
                <a class="" href="{{ url('/') }}">
                              <i class="icon_house_alt"></i>
                              <span>Pengumuman</span>
                          </a>
              </li>

      @if (Auth::check())
          @if(Auth::user()->isAdmin())
              @can('isAdmin',Auth::user())  
              <li class="sub-menu">
                    
                  <a href="javascript:;" class="">
                      <i class="icon_document_alt"></i>
                      <span>Register</span>
                      <span class="menu-arrow arrow_carrot-right"></span>
                  </a>
                  <ul class="sub">
                    <li><a class="" href="{{url('admin/create')}}">Register Admin</a></li>
                    <li><a class="" href="{{url('dosen/create')}}">Register Dosen</a></li>
                    <li><a class="" href="{{url('mahasiswa/create')}}">Register Mahasiswa</a></li>
                    <li><a class="" href="{{url('ruangan/create')}}">Register Ruangan</a></li>
                    <li><a class="" href="{{url('jurusan/create')}}">Register Jurusan</a></li>
                  </ul>
                
              </li>

              <li>
                <a class="" href="{{url('dosenMatkul/create')}}">
                    <i class="icon_desktop"></i>
                    <span>Daftar Dosen MK</span>
                </a>
              </li>

              <li>
                <a class="" href="{{url('jurusanMatakuliah/create')}}">
                    <i class="icon_desktop"></i>
                    <span>Daftar Jurusan MK</span>
                </a>
              </li>
              @endcan
          @endif

          @if(Auth::user()->isMahasiswa())
              <li>
                <a class="" href="{{url('fpp/create')}}">
                    <i class="icon_piechart"></i>
                    <span>Daftar Mata Kuliah</span>
                </a>
              </li>
          @elseif(Auth::user()->isDosen())
              <li>
                <a class="" href="{{url('kasuskhusus')}}">
                    <i class="icon_piechart"></i>
                    <span>Kasus Khusus</span>
                </a>
              </li>
          @endif
              <li>
                <a class="" href="{{url('fpp')}}">
                  <i class="icon_piechart"></i>
                  <span>Perwalian</span>
                </a>
              </li>

              <li>
                <a class="" href="{{url('matakuliah')}}">
                    <i class="icon_piechart"></i>
                    <span>Informasi MK</span>
                </a>
              </li>


          @if(Auth::user()->isAdmin())
              <li>
                <a class="" href="{{url('jadwalPerwalian')}}">
                    <i class="icon_genius"></i>
                    <span>Jadwal Perwalian</span>
                </a>
              </li>
          @endif
          @if(Auth::user()->isMahasiswa())
              <li>
                <a class="" href="{{url('fpp')}}">
                    <i class="icon_genius"></i>
                    <span>History Perwalian</span>
                </a>
              </li>
          @endif

      @endif

              <!-- MENU JADWAL PASTI MUNCUL -->
              <li class="sub-menu">
                <a href="javascript:;" class="">
                              <i class="icon_documents_alt"></i>
                              <span>Jadwal</span>
                              <span class="menu-arrow arrow_carrot-right"></span>
                          </a>
                <ul class="sub">
                  <li><a href="{{url('jadwalMatkul')}}"><span>Jadwal Kuliah</span></a></li>
                  <li><a href="{{url('jadwalUjian')}}"><span>Jadwal Ujian</span></a></li>
                </ul>
              </li>
        </ul>
        <!-- sidebar menu end-->
      </div>
    </aside>

@endif
    <!--sidebar end-->

    <!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <!--overview start-->
        <div class="row">
          <div class="col-lg-12">
            <h3 class="page-header"><i class="fa fa-laptop"></i> @yield('nama')</h3>
            <ol class="breadcrumb">
              <li><i class="fa fa-home"></i><a href="{{ url('/') }}">Home</a></li>
              <li><i class="fa fa-laptop"></i>@yield('nama')</li>
            </ol>
          </div>
        </div>


        <div id="isi-content" class="row">
          <div class="col-lg-12">
            @yield('pilih')
            @yield('content')
          </div>
        </div>

        

        </div>
        <!--/.row-->


        

                </div>
                <div class="widget-foot">
                  <!-- Footer goes here -->
                </div>
              </div>
            </div>

          </div>

        </div>
        <!-- project team & activity end -->

      </section>
      <div class="text-right">
        <div class="credits">
          <!--
            All the links in the footer should remain intact.
            You can delete the links only if you purchased the pro version.
            Licensing information: https://bootstrapmade.com/license/
            Purchase the pro version form: https://bootstrapmade.com/buy/?theme=NiceAdmin
          -->
          
        </div>
      </div>
    </section>
    <!--main content end-->
  </section>
  <!-- container section start -->

  <!-- javascripts -->
  <!-- <script src="{{ asset('js/app.js') }}"></script> -->
  <script src="{{  asset('css/js/jquery.js')}}"></script>
  <script src="{{  asset('css/js/jquery-ui-1.10.4.min.js')}}"></script>
  <script src="{{  asset('css/js/jquery-1.8.3.min.js')}}"></script>
  <script type="text/javascript" src="{{  asset('css/js/jquery-ui-1.9.2.custom.min.js')}}"></script>
  <!-- bootstrap -->
  <script src="{{  asset('css/js/bootstrap.min.js')}}"></script>
  <!-- nice scroll -->
  <script src="{{  asset('css/js/jquery.scrollTo.min.js')}}"></script>
  <script src="{{  asset('css/js/jquery.nicescroll.js')}}" type="text/javascript"></script>
  <!-- charts scripts -->
  <script src="{{  asset('css/asset/jquery-knob/js/jquery.knob.js')}}"></script>
  <script src="{{  asset('css/js/jquery.sparkline.js')}}" type="text/javascript"></script>
  <script src="{{  asset('css/  asset/jquery-easy-pie-chart/jquery.easy-pie-chart.js')}}"></script>
  <script src="{{  asset('css/js/owl.carousel.js')}}"></script>
  <!-- jQuery full calendar -->
  <<script src="{{  asset('css/js/fullcalendar.min.js')}}"></script>
    <!-- Full Google Calendar - Calendar -->
    <script src="{{  asset('css/asset/fullcalendar/fullcalendar/fullcalendar.js')}}"></script>
    <!--script for this page only-->
    <script src="{{  asset('css/js/calendar-custom.js')}}"></script>
    <script src="{{  asset('css/js/jquery.rateit.min.js')}}"></script>
    <!-- custom select -->
    <script src="{{  asset('css/js/jquery.customSelect.min.js')}}"></script>
    <script src="{{  asset('css/  asset/chart-master/Chart.js')}}"></script>

    <!--custome script for all page-->
    <script src="{{  asset('css/js/scripts.js')}}"></script>
    <!-- custom script for this page-->
<!--     <script src="{{  asset('css/js/sparkline-chart.js')}}"></script>
    <script src="{{  asset('css/js/easy-pie-chart.js')}}"></script>
    <script src="{{  asset('css/js/jquery-jvectormap-1.2.2.min.js')}}"></script>
    <script src="{{  asset('css/js/jquery-jvectormap-world-mill-en.js')}}"></script>
    <script src="{{  asset('css/js/xcharts.min.js')}}"></script>
    <script src="{{  asset('css/js/jquery.autosize.min.js')}}"></script>
    <script src="{{  asset('css/js/jquery.placeholder.min.js')}}"></script>
    <script src="{{  asset('css/js/gdp-data.js')}}"></script>
    <script src="{{  asset('css/js/morris.min.js')}}"></script>
    <script src="{{  asset('css/js/sparklines.js')}}"></script>
    <script src="{{  asset('css/js/charts.js')}}"></script>
    <script src="{{  asset('css/js/jquery.slimscroll.min.js')}}"></script> -->
    
    <script>
      //knob
      $(function() {
        $(".knob").knob({
          'draw': function() {
            $(this.i).val(this.cv + '%')
          }
        })
      });

      //carousel
      $(document).ready(function() {
        var timestamp="";
            function updateTime(){
                $('#time').html(Date(timestamp));
                timestamp++;
            }
            $(function()
            {
                
                setInterval(updateTime,1000);
            });

            $('select[name="kodematkul"]').on('change',function()
            {
                var kodematkul = $(this).val();
                
                $("#kp").empty();
                $.get('{{url("/fpp/ajax/")}}/'+kodematkul,function(data)
                {
                    if(data.kp!=null)
                    {
                        for(i=0;i<data.kp.length;i++)
                        {
                            $("#kp").append("<option value=\""+data.kp[i].KP+"\">"+data.kp[i].KP+"</option>");
                        }
                    }
                });
            });

            // $('#kk_idMhs').on('change',function()
            // {
            //     var id=$(this).val();
            //     alert(id);
            //     $("#link_kk_id").attr("href","{{url('kasuskhusus/')}}/"+id);
            // });


            // DARI BOOTSTRAP LANGSUNG
        $("#owl-slider").owlCarousel({
          navigation: true,
          slideSpeed: 300,
          paginationSpeed: 400,
          singleItem: true

        });
      });

      //custom select box

      $(function() {
        $('select.styled').customSelect();
      });

      /* ---------- Map ---------- */
      $(function() {
        $('#map').vectorMap({
          map: 'world_mill_en',
          series: {
            regions: [{
              values: gdpData,
              scale: ['#000', '#000'],
              normalizeFunction: 'polynomial'
            }]
          },
          backgroundColor: '#eef3f7',
          onLabelShow: function(e, el, code) {
            el.html(el.html() + ' (GDP - ' + gdpData[code] + ')');
          }
        });
      });

    </script>

</body>

</html>
