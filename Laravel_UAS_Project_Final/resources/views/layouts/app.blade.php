<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Perwalian @yield('title')</title>

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    
    <script type="text/javascript" src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
    <script type="text/javascript" src="{{asset('js/jquery-ui.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function()
        {
            var timestamp="";
            function updateTime(){
                $('#time').html(Date(timestamp));
                timestamp++;
            }
            $(function()
            {
                
                setInterval(updateTime,1000);
            });

            $('select[name="kodematkul"]').on('change',function()
            {
                var kodematkul = $(this).val();
                
                $("#kp").empty();
                $.get('{{url("/fpp/ajax/")}}/'+kodematkul,function(data)
                {
                    if(data.kp!=null)
                    {
                        for(i=0;i<data.kp.length;i++)
                        {
                            $("#kp").append("<option value=\""+data.kp[i].KP+"\">"+data.kp[i].KP+"</option>");
                        }
                    }
                });
            });

            $('#kk_idMhs').on('change',function()
            {
                var id=$(this).val();
                alert(id);
                $("#link_kk_id").attr("href","{{url('kasuskhusus/')}}/"+id);
            });
        });
    </script>
    <style type="text/css">
        a:link#judul,  a:visited#judul{
            text-decoration: none;
            /*color: red;*/
        }    </style>
</head>
<body>
    <!-- ubah -->
    <div style="color: black; height: 70px; font-size: 50px; border-bottom: solid 5px blue">
        <a href="{{ url('/') }}" id="judul">Sistem Perencanaan Studi</a>
    </div>
    <div id="time"></div>
    
    <div>
        <div id="app" style="width: 70% ;height: 100%;float: left;">
            @yield('pengumuman')
            @yield('pilih')
            @yield('content')
        </div>

        <div class="flex-center position-ref full-height" style="width: 30%; float: left;">
                @if (Route::has('login'))
                    <div class="top-right links">
                        @if (Auth::check())
                        
                        <ul class="nav navbar-nav navbar-right">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                @if(Auth::user()->isAdmin())
                                    {{ Auth::user()->admin->nama }}
                                @elseif(Auth::user()->isMahasiswa())
                                    {{ Auth::user()->mahasiswa->nama }} -
    {{Auth::user()->mahasiswa->jurusan->nama_jurusan}}
                                @elseif(Auth::user()->isDosen())
                                    {{Auth::user()->dosen->nama}} 
                                @endif
                                <span class="caret"></span>
                            </a>
                            <div>
                                <!-- eron tambah -->
                                <hr style="border: solid 1px black">
                                <a href="{{url('jadwalMatkul')}}">Jadwal Kuliah</a> <br>
                                <a href="{{url('jadwalUjian')}}">Jadwal Ujian</a>
                            </div>
                            <ul class="dropdown-menu" role="menu">
                                <li>                                
                                    <a href="{{ url('/') }}">Home</a>
                            @if(Auth::user()->isAdmin())
                                @can('isAdmin',Auth::user())
                                    <a href="{{ url('/profil') }}">Register User</a>
                                    <a href="{{url('dosenMatkul/create')}}">Daftar Dosen Matakuliah</a>
                                @endcan
                            @endif

                            @if(Auth::user()->isMahasiswa())
                                <a href="{{url('fpp/create')}}">Daftar Mata Kuliah</a>
                            @elseif(Auth::user()->isDosen())
                                <a href="{{url('kasuskhusus')}}">Kasus Khusus</a>
                            @endif
                                <a href="{{url('fpp')}}">
                                Perwalian</a>
                                <a href="{{url('matakuliah')}}">Informasi Mata Kuliah</a>

                            @if(Auth::user()->isAdmin())
                            <a href="{{url('jadwalPerwalian')}}">
                                Jadwal Perwalian</a>
                            @endif
                            @if(Auth::user()->isMahasiswa())
                                <a href="{{url('fpp')}}">History Perwalian</a>
                            @endif
                                <a href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                                </li>
                            </ul>
                            </li>
                        </ul>
                        @else
                            <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                                <label for="username" class="col-md-4 control-label">Username</label>

                                <div class="col-md-6">
                                    <input id="username" type="username" class="form-control" name="username" value="{{ old('username') }}" required autofocus>

                                    @if ($errors->has('username'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('username') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="col-md-4 control-label">Password</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control" name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Login
                                    </button>
                                </div>
                            </div>
                            <!-- <div>
                                @yield('pilih')
                            </div> -->
                            <div>
                                <!-- eron tambah -->
                                <hr style="border: solid 1px black">
                                <a href="{{url('jadwalMatkul')}}">Jadwal Kuliah</a> <br>
                                <a href="{{url('jadwalUjian')}}">Jadwal Ujian</a>
                            </div>
                        </form>
                        @endif
                      
                    </div>
                    <div class="top-left">
                        
                    </div>
                @endif
            </div>
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
