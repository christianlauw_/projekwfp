@section('nama', 'Tambah Jadwal MataKuliah')
@extends('layouts.index')

@section('content')
<form method="post" action="{{url('jadwalMatkul')}}">
<label style="width: 10%">MataKuliah :</label>
	{{csrf_field()}}
<select name="matkul">
	<option value="">--pilih mata kuliah--</option>
	@foreach($mataKuliah as $item)
		<option value="{{$item->id}}" name="tampung_sks-{{$item->id}}" sks="{{$item->sks}}">{{$item->kode_matkul}} - {{$item->nama}}</option>
	@endforeach
</select><br/>
<label style="width: 10%">Jumlah sks :</label>
<label name="sks">
	<input type="text" id="sks" disabled value="">
</label><br/>
<label style="width: 10%">Jadwal 1 :</label>
<select name="jadwal1">
	@foreach($jadwalKuliah as $item)
		<option value="{{$item->id}}">{{$item->hari}} - {{$item->jam_mulai}} - {{$item->jam_selesai}}</option>
	@endforeach
</select><br/>
<label style="width: 10%">Ruangan 1 :</label>
<select name="ruangan1">
	@foreach($ruangan as $item)
		<option value="{{$item->id}}">{{$item->nama_ruangan}} - {{$item->kapasitas}}</option>
	@endforeach
</select><br/>

<div id="jadwalke2" hidden>
	<label style="width: 10%">Jadwal 2 :</label>
	<select name="jadwal2">
		@foreach($jadwalKuliah as $item)
			<option value="{{$item->id}}">{{$item->hari}} - {{$item->jam_mulai}} - {{$item->jam_selesai}}</option>
		@endforeach
	</select><br/>
	<label style="width: 10%">Ruangan 2 :</label>
	<select name="ruangan2">
		@foreach($ruangan as $item)
			<option value="{{$item->id}}">{{$item->nama_ruangan}} - {{$item->kapasitas}}</option>
		@endforeach
	</select><br/>
</div>
<label style="width: 10%">KP :</label> <input type="text" name="KP"/>
<input type="submit" name=""/>
<script type="text/javascript" src="{{asset('/js/my.js')}}"></script>
@endsection