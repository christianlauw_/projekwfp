@section('nama', 'Edit Jadwal Mata Kuliah')
@extends('layouts.index')

@section('content')
<?php $x=0; ?>

<form method="post" action="{{url('/jadwalMatkul/'.$id.'/'.$kp)}}">
{{csrf_field()}}
<input name="_method" type="hidden" value="PATCH">
@foreach($matkul as $item1)
	@if($x==0)
		MataKuliah :
		<select disabled name="matkul">
			@foreach($mataKuliah as $item)
				<option value="{{$item->id}}"  {{$item1->id==$item->id ? "selected" : ""}}>{{$item->kode_matkul}} - {{$item->nama}}</option>
			@endforeach
		</select><br/>
		
		Jadwal 1 :
		<select name="jadwal1">
			@foreach($jadwalKul as $item)
				<option value="{{$item->id}}" {{$item1->id_jad==$item->id ? "selected" : ""}} >{{$item->hari}} - {{$item->jam_mulai}} - {{$item->jam_selesai}}</option>
			@endforeach
		</select><br/>
		Ruangan 1 :
		<select name="ruangan1">
			@foreach($ruang as $item)
				<option value="{{$item->id}}" {{$item1->id_ruang==$item->id ? "selected" : ""}}>{{$item->nama_ruangan}} - {{$item->kapasitas}}</option>
			@endforeach
		</select><br/>
	@elseif($x==1)
			Jadwal 2 :
			<select name="jadwal2">
				@foreach($jadwalKul as $item)
					<option value="{{$item->id}}" {{$item1->id_jad==$item->id ? "selected" : ""}} >{{$item->hari}} - {{$item->jam_mulai}} - {{$item->jam_selesai}}</option>
				@endforeach
			</select><br/>
			Ruangan 2 :
			<select name="ruangan2">
				@foreach($ruang as $item)
					<option value="{{$item->id}}" {{$item1->id_ruang==$item->id ? "selected" : ""}}>{{$item->nama_ruangan}} - {{$item->kapasitas}}</option>
				@endforeach
			</select><br/>
	@endif
	<?php $x++; ?>
	@if($x-1==0)
		KP : <input disabled type="text" name="KP" value="{{$item1->kp}}" /><br/>	
	@endif
@endforeach
<button type="submit">Update Kelas</button>
</form>
@endsection