@section('nama', 'Jadwal Mata Kuliah')
@extends('layouts.index')

@section('title', '- Jadwal Kuliah')
@section('pilih')
<br>

	<!-- <select name="pilihjurusan">
        <option value="Itdd">Information Technology Dual Degree</option>
        <option value="SI">Sistem Informasi</option>
        <option value="Elektro">Teknik Elektro</option>
        <option value="TI">Teknik Industri</option>
        <option value="IF">Teknik Informatika</option>
        <option value="Tekkim">Teknik Kimia</option>
        <option value="MM">Teknik Multimedia</option>
    </select> -->
    <br>

@endsection
@section('content')
<!-- <a href="{{url('/')}}">Home</a> -->
Search : <input type="text" name="" id="search_jadwalMatkul">
<table id="tabel_jadwalMatkul" class="table table-hover">
<thead>
<tr>
	<th>Hari</th>
	<th>Jam Masuk</th>
	<th>Jam Keluar</th>
	<th>Kode Matkul</th>
	<th>Nama</th>
	<th>SKS</th>
	<th>KP</th>
	<th>Ruangan</th>
	<th>Kapasitas</th>
	@if(Auth::check())
		@if(Auth::user()->isAdmin())
			<th>Edit</th>
		@endif
	@endif
</tr>
</thead>
<tbody>
@foreach($matkul as $item)
	<tr>
		<td>{{$item->hari}}</td>
		<td>{{$item->jam_masuk}}</td>
		<td>{{$item->jam_selesai}}</td>
		<td>{{$item->kode_matkul}}</td>
		@if(Auth::check())
			<td><a href="{{action('MatakuliahController@show',$item->id_matkul)}}">{{$item->nama}}</a></td>
		@else
			<td>{{$item->nama}}</td>
		@endif
		<td>{{$item->sks}}</td>
		<td>{{$item->kp}}</td>
		<td>{{$item->nama_ruang}}</td>
		<td>{{$item->kapasitas}}</td>
		@if(Auth::check())
				@if(Auth::user()->isAdmin())
					<td><a href="{{url('/jadwalMatkul/'.$item->id_matkul.'/edit/'.$item->kp)}}">Edit</a>
					</td>
				@endif
		@endif
	</tr>
@endforeach
</tbody>
</table>

@if(Auth::check())
	@if(Auth::user()->isAdmin())
		<a href="{{url('jadwalMatkul/create')}}">Create Jadwal Mata Kuliah</a>
	@endif
@endif

@endsection