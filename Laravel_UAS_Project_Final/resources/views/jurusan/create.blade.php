@section('nama', 'Register Jurusan')
@extends('layouts.index')

@section('content')
<h1>Jurusan Baru</h1>
@if($errors->any())
  <div class="alert alert-danger">
    <ul>
      @foreach($errors->all() as $error)
        <li>{{$error}}</li>
      @endforeach
    </ul>
  </div>
@endif
<form method="post" action="{{url('jurusan')}}">
	{{csrf_field()}}
	<label for="kode_jurusan" style="width:20%">Kode Jurusan :</label> <input type="text" name="kode_jurusan" id="kode_jurusan"><br/>
	<label for="nama_jurusan" style="width:20%">Nama Jurusan :</label> <input type="text" name="nama_jurusan" id="nama_jurusan"><br/>
	<input type="submit" class="btn btn-primary" value="Tambah Jurusan"/>
</form>


	<table class="table table-hover">
	<tr>
		<th>Kode Jurusan</th>
		<th>Nama Jurusan</th>
		@if(Auth::user()->isAdmin())
			<th>Edit</th>
			<th>Delete</th>
		@endif
	</tr>
	@foreach($jurusan as $item)
		<tr>
			<td>{{$item->kode_jurusan}}</td>
			<td>{{$item->nama_jurusan}}</td>
			@if(Auth::user()->isAdmin())
				<td><a class="btn btn-xs btn-success" href="{{action('JurusanController@edit', $item->id)}}" name="edit_jurusan" id_jurusan="{{$item->id}}">Edit</a></td>
				<td><form action="{{action('JurusanController@destroy',$item->id)}}" method="post">
					{{csrf_field()}}
		         	<input name="_method" type="hidden" value="DELETE"/>	
		         	<button type="submit" class="btn btn-xs btn-danger btn_delete">Delete</button>  	
				</form></td>
			@endif
		</tr>
		<tr>
			<td></td>
			<td colspan="3">
				<div class="jurusan-{{$item->id}}"></div>
			</td>
		</tr>
	@endforeach
	</table>
	<script type="text/javascript" src="{{asset('/js/my.js')}}"></script>

@endsection