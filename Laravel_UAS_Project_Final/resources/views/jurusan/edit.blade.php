<!-- section('nama', 'Edit Jurusan') -->
<!-- extends('layouts.index') -->

<!-- section('content') -->
<!-- <h1>Ubah Data Jurusan</h1> -->
@if($errors->any())
  <div class="alert alert-danger">
    <ul>
      @foreach($errors->all() as $error)
        <li>{{$error}}</li>
      @endforeach
    </ul>
  </div>
@endif

<form method="post" action="{{action('JurusanController@update', $id)}}">
  {{csrf_field()}}
  <input name="_method" type="hidden" value="PATCH">
  <label style="width: 20%;">Kode Jurusan </label><input type="text" name="kode_jurusan" disabled value="{{$jurusan->kode_jurusan}}"><br/>
  <label style="width: 20%;">Nama Jurusan : </label><input type="text" name="nama_jurusan" value="{{$jurusan->nama_jurusan}}"><br/>
  <input type="submit" class="btn" value="Ubah data Jurusan"/>
</form>
<!-- endsection -->
