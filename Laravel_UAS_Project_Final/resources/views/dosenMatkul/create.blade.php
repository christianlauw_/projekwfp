@section('nama', 'Daftar Dosen Mata Kuliah')
@extends('layouts.index')

@section('content')
<form method="post" action="{{url('dosenMatkul')}}">
	{{csrf_field()}}
	<label for="dosen" style="width:20%">Nama Dosen :</label>
	<select name="dosen">
		<option>--Pilih Dosen--</option>
		@foreach($dosen as $item)
			<option value="{{$item->id}}">{{$item->npk}} - {{$item->nama}}</option>
		@endforeach
	</select><br/>

	<label for="matkul" style="width:20%">Mata Kuliah :</label>
	<select name="matkul">
		<option>--Pilih Mata Kuliah--</option>
		@foreach($matkul as $item)
			<option value="{{$item->id}}">{{$item->kode_matkul}} - {{$item->nama}}</option>
		@endforeach
	</select><br/>
<input type="submit" name="" class="btn btn-primary" value="Tambah Pengajar">
</form>
<table class="table table-hover">
	<tr>
		<th>MataKuliah</th>
		<th>Dosen</th>
		<th>Action</th>
	</tr>
@foreach($matkul as $item)
	<tr>
		<td>{{$item->kode_matkul}} - {{$item->nama}}</td>
		
		<td>
			@foreach($item->dosens as $dos)
				{{$dos->nama}}<br>
			@endforeach
		</td>

		<td>
		@foreach($item->dosens as $dos)
			<form action="{{url('dosenMatkul/'.$dos->id.'/'.$item->id)}}" method="post">
					{{csrf_field()}}
		         	<input name="_method" type="hidden" value="DELETE"/>	
		         	<button type="submit" class="btn btn-xs btn-danger btn_delete">Delete</button><br>
				</form>
		@endforeach
		</td>
		
	</tr>
@endforeach
</table>
@endsection