@section('nama', 'Jadwal Perwalian')
@extends('layouts.index')

@section('content')
<table class="table table-hover">
	<tr>
		<th>No</th>
		<th>Nama_Sesi</th>
		<th>Tanggal Mulai</th>
		<th>Tanggal Selesai</th>
		<th>Sudah dijalankan</th>
		<th>Dibuat</th>
		<th>Diubah</th>
		
		<th>Edit</th>
	</tr>
@foreach($sesi as $item)
<tr>
	<td>{{$item->id}}</td>
	<td>{{$item->nama_sesi}}</td>
	<td>{{$item->start_input}}</td>
	<td>{{$item->finish_input}}</td>
	@if($item->isDone)
		<td>Finished</td>
	@else
		<td>Not Started</td>
	@endif
	<td>{{$item->created_at}}</td>
	<td>{{$item->updated_at}}</td>
	<td><a href="{{action('JadwalPerwalianController@edit',$item->id)}}">Edit</a></td>
</tr>
@endforeach
</table>
@endsection