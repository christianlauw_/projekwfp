@section('nama', 'Edit Jadwal Perwalian')
@extends('layouts.index')

@section('content')

	<form method="post" action="{{action('JadwalPerwalianController@update', $id)}}">
      {{csrf_field()}}
      <input name="_method" type="hidden" value="PATCH">
		Nama Sesi: <input type="text" name="nama_sesi"  value="{{$sesi->nama_sesi}}"><br/>

		Start Input: <input type="text" name="start_input" value="{{$sesi->start_input}}"/><br/>
		Finish Input: <input type="text" name="finish_input" value="{{$sesi->finish_input}}"/><br/>
		Sudah Dijalankan
		<input id="chkbox_isDone" type="checkbox" name="isDone" {{$sesi->isDone == 1 ? 'checked' : ''}} ><br/>
		<button type="submit" class="btn btn-primary">Update</button>
	</form>
	
@endsection