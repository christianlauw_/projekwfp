<!-- section('nama', 'Edit Mahasiswa') -->
<!-- extends('layouts.index') -->

<!-- section('content') -->
<!-- <h1>Mahasiswa Baru</h1> -->
@if($errors->any())
  <div class="alert alert-danger">
    <ul>
      @foreach($errors->all() as $error)
        <li>{{$error}}</li>
      @endforeach
    </ul>
  </div>
@endif

<form method="post" action="{{action('MahasiswaController@update', $id)}}">
	{{csrf_field()}}
	<input name="_method" type="hidden" value="PATCH">
	<label style="width: 20%;">NRP = Username: </label><input disabled="" type="text" value="{{$mhs->nrp}}"name="nrp"><br/>
	<label style="width: 20%;">Nama: </label><input type="text" disabled name="nama" value="{{$mhs->nama}}"><br/>
	<label style="width: 20%;">IPK : </label><input type="text" disabled name="ipk" value="{{$mhs->ipk}}"><br/>
	<label style="width: 20%;">IPS : </label><input type="text" disabled name="ips" value="{{$mhs->ips}}"><br/>
	<label style="width: 20%;">Jurusan :</label>
		<select name="jurusan">
			@foreach($jur as $item)
				<option value="{{$item->id}}" {{$mhs->jurusan_id == $item->id ? 'selected="selected"' : ''}} >{{$item->nama_jurusan}}</option>
			@endforeach
		</select><br/>
	<label style="width: 20%;">SKS : </label><input type="number" min="0" max="24" name="sks" value="{{$mhs->sks}}"><br/>
	<label style="width: 20%;">Tahun Masuk : </label><input type="number" name="tahunMasuk" min="<?php echo date("Y")-7;?>" value="{{$mhs->tahunMasuk}}"><br/>
	Asdos
		<input type="checkbox" name="isAsdos" {{$mhs->isAsdos == 1 ? 'checked' : ''}} ><br/>
	<input type="submit" class="btn" value="Ubah data Mahasiswa"/>
</form>
<!-- endsection -->
