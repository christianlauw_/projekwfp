@section('nama', 'Register Mahasiswa')
@extends('layouts.index')

@section('content')
<h1>Mahasiswa Baru</h1>
@if($errors->any())
  <div class="alert alert-danger">
    <ul>
      @foreach($errors->all() as $error)
        <li>{{$error}}</li>
      @endforeach
    </ul>
  </div>
@endif

<form method="post" action="{{url('mahasiswa')}}">
	{{csrf_field()}}
	<label for="nrp" style="width:20%">NRP = Username: </label><input type="text" name="nrp" id="nrp"><br/>
	<label for="nama" style="width:20%">Nama: </label><input type="text" name="nama" id="nama"><br/>
	<label for="ipk" style="width:20%">IPK : </label><input type="text" name="ipk" id="ipk"><br/>
	<label for="ips" style="width:20%">IPS : </label><input type="text" name="ips" id="ips"><br/>
	<label for="pass" style="width:20%">Password :</label><input type="password" name="password" id="pass"><br/>
	<label for="jurusan" style="width:20%">Jurusan :</label>
		<select name="jurusan">
			@foreach($jur as $item)
				<option value="{{$item->id}}">{{$item->nama_jurusan}}</option>
			@endforeach
		</select><br/>
	<label for="sks" style="width:20%">SKS : </label><input type="number" min="0" max="24" name="sks" id="sks"><br/>
	<label for="tahunMasuk" style="width:20%">Tahun Masuk : </label><input type="number" name="tahunMasuk" id="tahunMasuk" min="<?php echo date("Y");?>"><br/>
	<input type="submit" class="btn btn-primary" value="Tambah Mahasiswa"/>
	<br>
</form>
<table class="table table-hover">
	<tr>
		<th>NRP</th>
		<th>Nama</th>
		<th>Tahun Masuk</th>
		@if(Auth::user()->isAdmin())
			<th>Edit</th>
			<th>Delete</th>
		@endif
	</tr>
	@foreach($mhs as $item)
		<tr>
			<td>{{$item->nrp}}</td>
			<td>{{$item->nama}}</td>
			<td>{{$item->tahunMasuk}}</td>
			@if(Auth::user()->isAdmin())
				<td><a class="btn btn-xs btn-success" href="{{action('MahasiswaController@edit', $item->id)}}" name="edit_mhs" id_mhs="{{$item->id}}">Edit</a></td>
				<td><form action="{{action('MahasiswaController@destroy',$item->id)}}" method="post">
					{{csrf_field()}}
		         	<input name="_method" type="hidden" value="DELETE"/>
		         		
		         	<button type="submit" class="btn btn-xs btn-danger btn_delete">Delete</button>
		         	
				</form></td>
			@endif
		</tr>
		<tr>
			<td></td>
			<td colspan="4">
				<div class="mhs-{{$item->id}}"></div>
			</td>
		</tr>
	@endforeach
</table>
<script type="text/javascript" src="{{asset('/js/my.js')}}"></script>
@endsection
