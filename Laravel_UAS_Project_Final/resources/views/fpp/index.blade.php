@section('nama', 'FPP')
@extends('layouts.index')
@section('content')
@if(Auth::user()->isMahasiswa())
@else
	<button style="float: right;" id="btn-search-status">Search</button>
	<select id="select-status" style="float: right;">
					<option value="pending">Pending</option>
					<option value="ditolak">Ditolak</option>
					<option value="dicancel">Dicancel</option>
					<option value="diterima">Diterima</option>
				</select>
@endif			
@foreach($sesi as $urutan)
	@if($urutan->id==3)
		Kasus Khusus
	@else
		FPP - {{$urutan->id}}
	@endif
	@if($urutan->id!=3)
		@if(Auth::user()->isAdmin())
			@if(!$urutan->isDone)
				<a class="btn btn-primary" href="{{url('fpp/data/massUpdates/'.$urutan->id)}}">Start FPP-{{$urutan->id}} hitung</a>
			@endif
		@endif
		Search FPP {{$urutan->id}}: <input type="text" name="" id="search_fpp_{{$urutan->id}}">
		<table id="tabel_fpp_{{$urutan->id}}" class="table table-hover">
			<thead>
			<tr>
				@if(Auth::user()->isMahasiswa())
					<th>Matkul</th>
					<th>KP</th>
					<th>Status Input</th>
				@else
					<th>NRP</th>
					<th>Nama</th>
					<th>Matkul</th>
					<th>KP</th>
					<th>Jurusan</th>
					<th>Status Input</th>
				@endif
			</tr>
			</thead>
			<tbody>
			@foreach($fppRes as $item)
				@if($item->Sesi==$urutan->id)
					@if(Auth::user()->isMahasiswa())
						@if(Auth::user()->mahasiswa->id==$item->mhs_id)
							@if($item->statInput=='0')
								<tr class="warning" nama="status">
							@elseif($item->statInput=='0.5')
								<tr class="danger" nama="status">
							@elseif($item->statInput=='0.75')
								<tr class="table-info" nama="status">
							@else
								<tr class="success" nama="status">
							@endif
								<td>{{$item->matkul}}</td>
								<td>{{$item->KP}}</td>
								@if($item->statInput=='0')
									<td>Pending</td>
								@elseif($item->statInput=='0.5')
									<td>Ditolak</td>
								@elseif($item->statInput=='0.75')
									<td>Dicancel</td>
								@else
									<td>Diterima</td>
								@endif
							</tr>
						@endif
					@else
						@if($item->statInput=='0')
							<tr class="warning" nama="status">
						@elseif($item->statInput=='0.5')
							<tr class="danger" nama="status">
						@elseif($item->statInput=='0.75')
							<tr class="table-info" nama="status">
						@else
							<tr class="success" nama="status">
						@endif
							<td>{{$item->nrp}}</td>
							<td>{{$item->nama}}</td>
							<td>{{$item->matkul}}</td>
							<td>{{$item->KP}}</td>
							<td>{{$item->jurs}}</td>
							@if($item->statInput=='0')
								<td class="stats-pending">Pending</td>
							@elseif($item->statInput=='0.5')
								<td class="stats-ditolak">Ditolak</td>
							@elseif($item->statInput=='0.75')
								<td class="stats-dicancel">Dicancel</td>
							@else
								<td class="stats-diterima">Diterima</td>
							@endif
						</tr>
					@endif
				@endif
			@endforeach
			</tbody>
		</table><br/>
	@elseif($urutan->id==3)
		<b>Search Kasus Khusus : <input type="text" name="" id="search_fpp_{{$urutan->id}}"></b>
		<table id="tabel_fpp_{{$urutan->id}}"" class="table">
			<thead>
			<tr>
				@if(Auth::user()->isMahasiswa())
					<th>Matkul</th>
					<th>KP</th>
					<th>Status Input</th>
				@else
					<th>NRP</th>
					<th>Nama</th>
					<th>Matkul</th>
					<th>KP</th>
					<th>Jurusan</th>
					<th>Status Input</th>
				@endif
			</tr>
			</thead>
			<tbody>
			@foreach($fppRes as $item)
				@if($item->Sesi==$urutan->id)
					@if(Auth::user()->isMahasiswa())
						@if(Auth::user()->mahasiswa->id==$item->mhs_id)
							@if($item->statInput=='0')
								<tr class="warning" nama="status">
							@elseif($item->statInput=='0.5')
								<tr class="danger" nama="status">
							@elseif($item->statInput=='0.75')
								<tr class="table-info" nama="status">
							@else
								<tr class="success" nama="status">
							@endif
								<td>{{$item->matkul}}</td>
								<td>{{$item->KP}}</td>
								@if($item->statInput=='0')
									<td>Pending</td>
								@elseif($item->statInput=='0.5')
									<td>Ditolak</td>
								@elseif($item->statInput=='0.75')
									<td>Dicancel</td>
								@else
									<td>Diterima</td>
								@endif
							</tr>
						@endif
					@else
						@if($item->statInput=='0')
							<tr class="warning" nama="status">
						@elseif($item->statInput=='0.5')
							<tr class="danger" nama="status">
						@elseif($item->statInput=='0.75')
							<tr class="info" nama="status">
						@else
							<tr class="success" nama="status">
						@endif
							<td>{{$item->nrp}}</td>
							<td>{{$item->nama}}</td>
							<td>{{$item->matkul}}</td>
							<td>{{$item->KP}}</td>
							<td>{{$item->jurs}}</td>
							@if($item->statInput=='0')
								<td>Pending</td>
							@elseif($item->statInput=='0.5')
								<td>Ditolak</td>
							@elseif($item->statInput=='0.75')
								<td>Dicancel</td>
							@else
								<td>Diterima</td>
							@endif
						</tr>
					@endif
				@endif
			@endforeach
			</tbody>
		</table>
	@endif
@endforeach
<script type="text/javascript" src="{{asset('/js/my.js')}}"></script>
@endsection