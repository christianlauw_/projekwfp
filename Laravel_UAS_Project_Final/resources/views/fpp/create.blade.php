@section('nama', 'FPP')
@extends('layouts.index')
@section('content')
<h3>DAFTAR KELAS MATA KULIAH</h3>
<?php $statusOpen=0; 
	$date = new DateTime();
	$date->setTimezone(new DateTimeZone('Asia/Jakarta'));
	$time = $date->format('Y-m-d H:i:s')?> 
<br/>

@foreach($sesi as $itemSesi) 
	@if($itemSesi->start_input<=$time AND $itemSesi->finish_input>=$time) 
	 	@if($itemSesi->id!=3)
			<form method="post" action="{{url('/fpp')}}">
				{{csrf_field()}}

				<input type="hidden" name="sesi" value="{{$itemSesi->id}}">
				<input type="hidden" name="mhs" value="{{Auth::user()->mahasiswa->id}}">
				Pilih Matkul :
				<select name="kodematkul">
					<option value="">---Pilih Mata Kuliah---</option>
					@foreach($matkul as $item)
						<option value="{{$item->id_matkul}}">{{$item->kode_matkul}} - {{$item->nama}}</option>
					@endforeach
				</select>
				KP : 
				<select id="kp" name="kp">
				</select><br/>
				SKS Maks : {{Auth::user()->mahasiswa->sks}}<br/>
				SKS DIGUNAKAN : {{$sksDigunakan}}<br/>
				Kapasitas : <label id="kapa_KP"></label>
				Isi : <label id="kapa_isi_KP"></label><br/>
				<input type="submit" name="Daftar" value="Daftar">
			</form>
			@if(Auth::user()->isMahasiswa())
			<table class="table">
				<tr>
						<th>Kode Matkul</th>
						<th>Nama Matkul</th>
						<th>KP</th>
						<th>SKS</th>
						<th>status</th>
						<th>Delete</th>
				</tr>
				@foreach($fpp as $item)
					@if($item->sesi_id==$itemSesi->id)
					@if(Auth::user()->mahasiswa->id==$item->mhs_id)
					<tr>
						<td>
							{{$item->kode}}
						</td>
						<td>
							{{$item->nama}}
						</td>
						<td>
							{{$item->kp}}
						</td>
						<td>
							{{$item->sks}}
						</td>
						@if($item->status=='0')
							<td>Pending</td>
						@elseif($item->status=='0.5')
							<td>Ditolak</td>
						@elseif($item->status=='0.75')
							<td>Dicancel</td>
						@elseif($item->status=='1')	
							<td>Diterima</td>
						@endif
						<td>
							<form action="{{url('/fpp/'.Auth::user()->mahasiswa->id.'/'.$itemSesi->id.'/'.$item->id_matkul)}}" method="post">
		            			{{csrf_field()}}
		            			<input name="_method" type="hidden" value="DELETE">
		            			<button type="submit">Delete</button>
		          			</form>
						</td>
					</tr>
					@endif
					@endif
				@endforeach
			</table>
			@endif <!-- BUkan kk -->
		@else
		<form method="post" action="{{url('/kasuskhusus')}}">
				{{csrf_field()}}
				<input type="hidden" name="sesi" value="{{$itemSesi->id}}">
				<input type="hidden" name="mhs" value="{{Auth::user()->mahasiswa->id}}">
				Pilih Matkul :
				<select name="kodematkul">
					<option value="">---Pilih Mata Kuliah---</option>
					@foreach($matkul as $item)
						<option value="{{$item->id_matkul}}">{{$item->kode_matkul}} - {{$item->nama}}</option>
					@endforeach
				</select>
				KP : 
				<select id="kp" name="kp">
				</select>
				SKS Maks : {{Auth::user()->mahasiswa->sks}}<br/>
				SKS DIGUNAKAN : {{$sksDigunakan}}<br/>
				Kapasitas : <label id="kapa_KP"></label>
				Isi : <label id="kapa_isi_KP"></label><br/>
				<input type="submit" name="Daftar" value="Daftar">
		</form>
			@if(Auth::user()->isMahasiswa())
			<table class="table">
				<tr>
						<th>Kode Matkul</th>
						<th>Nama Matkul</th>
						<th>KP</th>
						<th>SKS</th>
						<th>status</th>
				</tr>
				@foreach($fpp as $item)
					@if($item->sesi_id==$itemSesi->id)
					<tr>
						<td>
							{{$item->kode}}
						</td>
						<td>
							{{$item->nama}}
						</td>
						<td>
							{{$item->kp}}
						</td>
						<td>
							{{$item->sks}}
						</td>
						@if($item->status=='0')
							<td>Pending</td>
						@elseif($item->status=='0.5')
							<td>Ditolak</td>
						@elseif($item->status=='0.75')
							<td>Dicancel</td>
						@elseif($item->status=='1')	
							<td>Diterima</td>
						@endif
					</tr>
					@endif
				@endforeach
			</table>
			@endif
		@endif

		<?php $statusOpen++; ?> 
	@endif
@endforeach
@if($statusOpen==0) 
  <p>Perwalian belum dibuka</p> 
  <h1>Jadwal Perwalian</h1> 
  <table class="table table-striped"> 
  <tr class="success"> 
    <th>No</th> 
    <th>Nama Sesi</th> 
    <th>Jam Mulai</th> 
    <th>Jam Selesai</th> 
  </tr> 
  @foreach($sesi as $it) 
  <tr class="active"> 
    <td>{{$it->id}}</td> 
    <td>{{$it->nama_sesi}}</td> 
    <td>{{$it->start_input}}</td> 
    <td>{{$it->finish_input}}</td> 
  </tr> 
  @endforeach 
  </table> 
@endif
@endsection