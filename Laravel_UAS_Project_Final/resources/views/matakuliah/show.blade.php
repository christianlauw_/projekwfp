@section('nama', 'MataKuliah')
@extends('layouts.index')

@section('content')
@if(Auth::check())
<table class="table table-hover">
	<tr>
		<th>Kode Matkul</th>
		<th>Nama Matkul</th>
		<th>KP</th>
		<th>Ruangan</th>
		<th>Hari</th>
		<th>Jam_Mulai</th>
		<th>Jam_Selesai</th>
	</tr>
	@foreach($showMatkul as $item)
	<tr>
		<td>{{$item->kode_matkul}}</td>
		<td>{{$item->nama_matkul}}</td>
		<td>{{$item->kp}}</td>
		<td>{{$item->nama_ruang}}</td>
		<td>{{$item->hari}}</td>
		<td>{{$item->jam_mulai}}</td>
		<td>{{$item->jam_selesai}}</td>
	</tr>
	@endforeach
</table><br/>
Pengajar
<table class="table table-hover">
	<tr>
	<th>Npk</th>
	<th>Nama</th>
	@if(Auth::user()->isAdmin())
		<th>Delete</th>
	@endif
	</tr>
	@foreach($dosenAjar->dosens as $dos)
	<tr>
		
		<td>{{$dos->npk}}</td>
		<td>{{$dos->nama}}</td>
		@if(Auth::user()->isAdmin())
		<td>			
         	<form action="{{url('/dosenMatkul/'.$dos->id.'/'.$id)}}" method="post">
            {{csrf_field()}}
            <input name="_method" type="hidden" value="DELETE">
            <button type="submit">Delete</button>
          </form>
		</td>
		@endif
	</tr>
	@endforeach
</table>
@endif
@endsection