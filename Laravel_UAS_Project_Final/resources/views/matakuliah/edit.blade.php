<!-- section('nama', 'Edit MataKuliah')
extends('layouts.index')

section('content') -->
<form method="post" action="{{action('MatakuliahController@update',$id)}}">
	{{csrf_field()}}
	<input name="_method" type="hidden" value="PATCH">
	<label style="width:20%;">Kode Matkul : </label><input type="text" name="kode_matkul" value=" {{$matkul->kode_matkul}}"><br/>
	<label style="width:20%;">Nama Matkul : </label><input type="text" name="nama" value=" {{$matkul->nama}}" style="width: 300px;" /><br/>
	<label style="width:20%;">Sks : </label><input min="2" max="6" type="number" name="sks" value="{{$matkul->sks}}"/><br/>
	<label style="width:20%;">Hari Ujian :</label>
	<select name="hari_ujian">
		@foreach($days as $item)
			<option value="{{$item->hari}}"{{$matkul->hari_ujian == $item->hari ? 'selected="selected"' : ''}} >{{$item->hari}}</option>
		@endforeach
	</select><br/>
	<label style="width:20%;">Minggu Ujian :</label>
	<select name="minggu_ujian">
		@for($i=1;$i<=4;$i++)
		<option value="{{$i}}" {{$matkul->minggu_juian == $i ? 'selected="selected"' : ''}} >{{$i}}</option>
		@endfor
	</select><br/>
	<label style="width:20%;">Jam Ujian :</label>
	<select name="jam_ujian">
		@for($i=1;$i<=4;$i++)
			<option value="{{$i}}" {{$matkul->jam_ujian == $i ? 'selected="selected"' : ''}} >{{$i}}</option>
		@endfor
	</select><br/>
	<label style="width:20%;">Semester</label><input type="number" min="1" max="8" name="semester" value="{{$matkul->semester}}"><br/>
	<input type="submit" name=""/>
</form>
<!-- endsection('content') -->