<!-- section('nama', 'Tambah MataKuliah') -->
<!-- extends('layouts.index') -->

<!-- section('content') -->
<form method="post" action="{{url('matakuliah')}}">
	{{csrf_field()}}
	<label for="kode_matkul" > Kode Matkul : <input type="text" name="kode_matkul" id="kode_matkul"><br/>
	<label for="nama" >Nama Matkul : <input type="text" name="nama" id="nama"/><br/>
	<label for="sks" >Sks : <input min="2" max="6" type="number" name="sks" id="sks"/><br/>
	<label for="hari_ujian" >Hari Ujian :
	<select name="hari_ujian" id="hari_ujian">
		@foreach($days as $item)
			<option value="{{$item->hari}}">{{$item->hari}}</option>
		@endforeach
	</select><br/>
	<label for="minggu_ujian" >Minggu Ujian :
	<select name="minggu_ujian" id="minggu_ujian">
		@for($i=1;$i<=4;$i++)
		<option value="{{$i}}">{{$i}}</option>
		@endfor
	</select><br/>
	<label for="jam_ujian" >Jam Ujian :
	<select name="jam_ujian" id="jam_ujian">
		@for($i=1;$i<=4;$i++)
			<option value="{{$i}}">{{$i}}</option>
		@endfor
	</select><br/>
	<label for="semester" >Semester<input type="number" min="1" max="8" name="semester" id="semester"><br/>
	<input type="submit" name="" value="Tambah Matkul" />
</form>
<!-- endsection -->