@section('nama', 'MataKuliah')
@extends('layouts.index')

@section('content')
@if(Auth::check())
@if(Auth::user()->isAdmin())
	<div id="container_create_matkul"></div><br>
	<a href="{{url('matakuliah/create')}}" class="btn btn-xs btn-info" id="create_matkul">Create Mata Kuliah</a>
@endif

Search : <input type="text" name="" id="search_mataKuliahList">
<table id="tabel_mataKuliahList" class="table table-hover">
<tr>
	<th>Kode_Matkul</th>
	<th>Nama Matkul</th>
	@if(Auth::user()->isAdmin())
		<th>Edit</th>
		<th>Delete</th>
	@endif
	<th>Show</th>
</tr>
@foreach($matkul as $item)
	<tr>
		<td>{{$item->kode_matkul}}</td>
		<td>{{$item->nama}}</td>
		@if(Auth::user()->isAdmin())
			<td><a href="{{action('MatakuliahController@edit',$item->id)}}" name="edit" class="btn btn-xs btn-success" id_ke="{{$item->id}}" >Edit</a></td>
			<td>
				<form action="{{action('MatakuliahController@destroy', $item->id)}}" method="post">
            		{{csrf_field()}}
            		<input name="_method" type="hidden" value="DELETE">
            		<button type="submit" class="btn btn-xs btn-danger btn_delete">Delete</button>
          		</form>
			</td>
		@endif
		<td><a href="{{action('MatakuliahController@show',$item->id)}}" class="btn btn-xs btn-info">Show</td>
	</tr>
	<tr>
		<td colspan="5">
			<div class="coba-{{$item->id}}"></div>
		</td>
	</tr>
	
@endforeach
</table>
@endif

<script type="text/javascript" src="{{asset('/js/my.js')}}"></script>
@endsection