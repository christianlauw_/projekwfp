<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    protected $fillable=['npk','nama','user_id'];

    public function user()
    {
    	return $this->belongsTo('App\User');
    }
}
