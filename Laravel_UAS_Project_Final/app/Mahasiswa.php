<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Mahasiswa extends Model
{
    protected $fillable=['nrp','nama','ipk','ips','sks','user_id','jurusan_id','isAsdos','tahunMasuk'];

    public function prioritas()
    {
    	return $this->belongsToMany('App\Prioritas','mahasiswas_prioritas','mahasiswa_id','prioritas_id')->withTimestamps();
    }
    public function sesis()
    {
    	return $this->belongsToMany('App\Sesi','mahasiswas_sesis_matakuliahs','mahasiswa_id','sesi_id')->withPivot('status_input')->withTimestamps();
    }
    public function matakuliahs()
    {
    	return $this->belongsToMany('App\MataKuliah','mahasiswas_sesis_matakuliahs','mahasiswa_id','matakuliah_id')->withPivot('status_input')->withTimestamps();
    }
    public function user()
    {
    	return $this->belongsTo('App\User');
    }
    public function jurusan()
    {
        return $this->belongsTo('App\Jurusan');
    }

    //Cek Mahasiswa Sudah Input? sehingga tidak bisa didelete admin
    public static function cekMahasiswa($idMhs)
    {
        DB::table('mahasiswas_sesis_matakuliahs')->where(['mahasiswa_id','=',$idMhs]);
    }
    public static function sksTerpakai($idMhs)
    {
        $res = MataKuliah::join('mahasiswas_sesis_matakuliahs','mata_kuliahs.id','=','mahasiswas_sesis_matakuliahs.matakuliah_id')
            ->where('mahasiswas_sesis_matakuliahs.mahasiswa_id','=',$idMhs)->whereIn('mahasiswas_sesis_matakuliahs.status_input',['0','1'])->sum('mata_kuliahs.sks');
        return $res;
    }
}
