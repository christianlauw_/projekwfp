<?php

namespace App\Policies;

use App\User;
use App\MataKuliah;
use Illuminate\Auth\Access\HandlesAuthorization;

class MataKuliahPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the mataKuliah.
     *
     * @param  \App\User  $user
     * @param  \App\MataKuliah  $mataKuliah
     * @return mixed
     */
    public function view(User $user, MataKuliah $mataKuliah)
    {
        //
    }

    /**
     * Determine whether the user can create mataKuliahs.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        $this->authorize('isAdmin',$user);
        return view('matakuliah.create');
    }

    /**
     * Determine whether the user can update the mataKuliah.
     *
     * @param  \App\User  $user
     * @param  \App\MataKuliah  $mataKuliah
     * @return mixed
     */
    public function update(User $user, MataKuliah $mataKuliah)
    {
        $this->authorize('isAdmin',$user);
        return true;
    }

    /**
     * Determine whether the user can delete the mataKuliah.
     *
     * @param  \App\User  $user
     * @param  \App\MataKuliah  $mataKuliah
     * @return mixed
     */
    public function delete(User $user, MataKuliah $mataKuliah)
    {
        $this->authorize('isAdmin',$user);
        return true;
    }
}
