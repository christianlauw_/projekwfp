<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Carbon\Carbon;
class MataKuliah extends Model
{
    protected $table = 'mata_kuliahs';
    protected $fillable=['nama','kode_matkul','semester','sks','hari_ujian','minggu_ujian','jam_ujian'];

    public function sesis()
    {
    	return $this->belongsToMany('App\Sesi','mahasiswas_sesis_matakuliahs','matakuliah_id','sesi_id')->withPivot('status_input')->withTimestamps();
    }
    public function mahasiswas()
    {
    	return $this->belongsToMany('App\Mahasiswa','mahasiswas_sesis_matakuliahs','matakuliah_id','mahasiswa_id')->withPivot('status_input')->withTimestamps();
    }

    public function dosens()
    {
    	return $this->belongsToMany('App\Dosen','dosens_matakuliahs','matakuliah_id','dosen_id')->withTimestamps();
    }

    public function jadwalkuliahs()
    {
    	return $this->belongsToMany('App\JadwalKuliah','jadwals_ruangans_matakuliahs','matakuliah_id','jadwalkuliah_id')->withPivot('KP')->withTimestamps();
    }
    public function ruangans()
    {
    	return $this->belongsToMany('App\JadwalKuliah','jadwals_ruangans_matakuliahs','matakuliah_id','ruangan_id')->withPivot('KP')->withTimestamps();
    }
    public function jurusans()
    {
        return $this->belongsToMany('App\Jurusan','jurusans_matakuliahs','matakuliah_id','jurusan_id')->withTimestamps();
    }

    //Ambil matkul tertentu untuk fungsi show custom
    public static function getMatkulById($id)
    {
        $showMatkul = MataKuliah::join('jadwals_ruangans_matakuliahs','jadwals_ruangans_matakuliahs.matakuliah_id','=','mata_kuliahs.id')
            ->join('jadwal_kuliahs','jadwals_ruangans_matakuliahs.jadwalkuliah_id','=','jadwal_kuliahs.id')
            ->join('ruangans','ruangans.id','=','jadwals_ruangans_matakuliahs.ruangan_id')
            ->where([
                ['mata_kuliahs.id', '=', $id],])
            ->select('mata_kuliahs.id as id_matkul','mata_kuliahs.kode_matkul as kode_matkul','mata_kuliahs.sks as sks','mata_kuliahs.nama as nama_matkul','ruangans.kapasitas as kapas','ruangans.nama_ruangan as nama_ruang','jadwals_ruangans_matakuliahs.KP as kp','jadwal_kuliahs.hari as hari','jadwal_kuliahs.jam_mulai as jam_mulai','jadwal_kuliahs.jam_selesai as jam_selesai','mata_kuliahs.semester as sem')
            ->get();
        return $showMatkul;
    }
    public static function getAllJadwalUjian()
    {
        return Matakuliah::join('days','days.hari','=','mata_kuliahs.hari_ujian')
        ->join('jadwals_ruangans_matakuliahs','jadwals_ruangans_matakuliahs.matakuliah_id','=','mata_kuliahs.id')
        ->orderBy('mata_kuliahs.minggu_ujian','asc')
        ->orderBy('days.id','asc')
        ->orderBy('mata_kuliahs.jam_ujian','asc')
        ->select('mata_kuliahs.minggu_ujian as mgg_ujian','mata_kuliahs.hari_ujian as hr_ujian','mata_kuliahs.jam_ujian as jm_ujian','mata_kuliahs.kode_matkul as kode_matkul','mata_kuliahs.nama as nama')->distinct()
        ->get();
    }
    //Hapus Jadwal Kuliah dari kp tertentu dari tabel jadwals_ruangans_matakuliahs
    public static function deleteJadwalKuliah($id,$kp)
    {
        $res = DB::table('jadwals_ruangans_matakuliahs')->where([
                ['matakuliah_id', '=', $id],
                ['KP', '=', $kp],
            ]);
        return $res;
    }
    public static function getAllJadwalKuliah()
    {
        $res = MataKuliah::join('jadwals_ruangans_matakuliahs','jadwals_ruangans_matakuliahs.matakuliah_id','=','mata_kuliahs.id')
                ->join('ruangans','jadwals_ruangans_matakuliahs.ruangan_id','=','ruangans.id')
                ->join('jadwal_kuliahs','jadwals_ruangans_matakuliahs.jadwalkuliah_id','=','jadwal_kuliahs.id')
                ->join('jurusans_matakuliahs','jurusans_matakuliahs.matakuliah_id','=','mata_kuliahs.id')
                ->join('jurusans','jurusans.id','=','jurusans_matakuliahs.jurusan_id')
                ->select('mata_kuliahs.id as id_matkul','mata_kuliahs.nama as nama','mata_kuliahs.sks as sks','jadwal_kuliahs.hari as hari','jadwal_kuliahs.jam_mulai as jam_masuk','jadwal_kuliahs.jam_selesai as jam_selesai','ruangans.nama_ruangan as nama_ruang','ruangans.kapasitas as kapasitas','jadwals_ruangans_matakuliahs.KP as kp','mata_kuliahs.kode_matkul as kode_matkul','jurusans.id as id_jur','mata_kuliahs.semester as sem')
                ->get();
        return $res;
    }
    //MataKuliah dengan kp tertentu yang akan di edit 
    public static function getMataKuliahtoEdit($id,$kp)
    {
        $res = MataKuliah::find($id)->join('jadwals_ruangans_matakuliahs','jadwals_ruangans_matakuliahs.matakuliah_id','=','mata_kuliahs.id')
        ->join('ruangans','ruangans.id','=','jadwals_ruangans_matakuliahs.ruangan_id')
        ->join('jadwal_kuliahs','jadwal_kuliahs.id','=','jadwals_ruangans_matakuliahs.jadwalkuliah_id')
        ->where([
                ['mata_kuliahs.id', '=', $id],
                ['jadwals_ruangans_matakuliahs.KP', '=', $kp],
            ])
        ->select('mata_kuliahs.id as id','mata_kuliahs.nama as nama','mata_kuliahs.kode_matkul as kode_matkul','jadwal_kuliahs.id as id_jad','jadwal_kuliahs.hari as hari','jadwal_kuliahs.jam_mulai as jam_mulai','jadwal_kuliahs.jam_selesai as jam_selesai','ruangans.nama_ruangan as nama_ruangan','ruangans.kapasitas as Kapasitas','jadwals_ruangans_matakuliahs.KP as kp','ruangans.id as id_ruang')
        ->get();
        return $res;
    }
    //Ambil semua fpp
    public static function getAllFPP()
    {
        return DB::table('mahasiswas_sesis_matakuliahs')
                ->join('mahasiswas','mahasiswas_sesis_matakuliahs.mahasiswa_id','=','mahasiswas.id')
                ->join('sesis','mahasiswas_sesis_matakuliahs.sesi_id','=','sesis.id')
                ->join('mata_kuliahs','mahasiswas_sesis_matakuliahs.matakuliah_id','=','mata_kuliahs.id')
                ->join('jurusans','mahasiswas.jurusan_id','=','jurusans.id')
                ->select('mahasiswas.nrp as nrp','mahasiswas.nama as nama','mata_kuliahs.nama as matkul','jurusans.nama_jurusan as jurs','mahasiswas_sesis_matakuliahs.status_input as statInput','sesis.id as Sesi','mahasiswas.id as mhs_id','mahasiswas_sesis_matakuliahs.kp as KP','mata_kuliahs.id as matakuliah_id')
                ->get();
    }

    //Ambil FPP mhs by id
    public static function getFPPMhs($id)
    {
        return DB::table('mahasiswas_sesis_matakuliahs')
                ->join('mahasiswas','mahasiswas_sesis_matakuliahs.mahasiswa_id','=','mahasiswas.id')
                ->join('sesis','mahasiswas_sesis_matakuliahs.sesi_id','=','sesis.id')
                ->join('mata_kuliahs','mahasiswas_sesis_matakuliahs.matakuliah_id','=','mata_kuliahs.id')->where('mahasiswas_sesis_matakuliahs.mahasiswa_id','=',$id)
                ->select('mahasiswas.nrp as nrp','mahasiswas.nama as nama','mata_kuliahs.nama as matkul','mahasiswas_sesis_matakuliahs.status_input as statInput','sesis.id as Sesi','mahasiswas.id as mhs_id','mahasiswas_sesis_matakuliahs.kp as KP','mata_kuliahs.id as matakuliah_id','mata_kuliahs.kode_matkul as kode','mata_kuliahs.sks as sks')
                ->get();
    }

    //Ambil KP dari matkul yang dibuka
    public static function getKPMatkul($id)
    {
        $res = DB::table('jadwals_ruangans_matakuliahs')->where('matakuliah_id',$id)->select('KP')->distinct()->get();
        return $res;
    }

    //Ambil matkul yang dibuka per matkul (asumsi yang punya jadwal berarti buka) sesuai jurusan mahasiswa
    public static function getMatkulBuka($idMhs)
    {
        $res = DB::table('jadwals_ruangans_matakuliahs')->join('mata_kuliahs','jadwals_ruangans_matakuliahs.matakuliah_id','=','mata_kuliahs.id')->join('jurusans_matakuliahs','jurusans_matakuliahs.matakuliah_id','=','mata_kuliahs.id')->join('jurusans','jurusans.id','=','jurusans_matakuliahs.jurusan_id')->join('mahasiswas','mahasiswas.jurusan_id','=','jurusans_matakuliahs.jurusan_id')->where(['mahasiswas.id'=>$idMhs])
        ->select('mata_kuliahs.nama as nama','mata_kuliahs.kode_matkul as kode_matkul','mata_kuliahs.id as id_matkul')->distinct()->get();
        return $res;
    }
    //Ambil matkul yang dibuka per KP
    public static function getMatkulBukaForFPP()
    {
        $res = DB::table('jadwals_ruangans_matakuliahs')->join('mata_kuliahs','jadwals_ruangans_matakuliahs.matakuliah_id','=','mata_kuliahs.id')->join('ruangans','ruangans.id','=','jadwals_ruangans_matakuliahs.ruangan_id')->select('mata_kuliahs.nama as nama','mata_kuliahs.kode_matkul as kode_matkul','mata_kuliahs.id as id_matkul','jadwals_ruangans_matakuliahs.ruangan_id as r_id','jadwals_ruangans_matakuliahs.kp as kp')->distinct()->get();
        return $res;
    }

    //Untuk cek input FPP yang sama dari mahasiswa dalam sesi tertentu(sesi = Untuk jaga"jaga kalau ada cancel jdi history yg lama tetap ada)
    public static function getInputMahasiswa($idMatkul,$idMhs)
    {
//         SELECT msm.*
// from mahasiswas_sesis_matakuliahs msm
// inner join mata_kuliahs mk on mk.id = msm.matakuliah_id
// inner join mahasiswas m on m.id = msm.mahasiswa_id
// where m.id=1 AND mk.id=2 AND msm.status_input IN('0','1')
        $res =  DB::table('mahasiswas_sesis_matakuliahs')->join('mata_kuliahs','mata_kuliahs.id','=','mahasiswas_sesis_matakuliahs.matakuliah_id')
        ->join('mahasiswas','mahasiswas.id','=','mahasiswas_sesis_matakuliahs.mahasiswa_id')
                ->where(
                [
                'mahasiswas.id'  => $idMhs,
                'mata_kuliahs.id' => $idMatkul,
            ])->whereIn('status_input',['0','1'])->get();
        //var_dump($res->count());
        if($res->count()>0)
            return true;
        else
            return false;
        //return $res;
    }

    //Hanya bisa delete yang pending
    public static function getInputMahasiswaToDelete($idMatkul,$idMhs,$idSesi)
    {
        $res =  DB::table('mahasiswas_sesis_matakuliahs')->where(
                [
                'mahasiswa_id'  => $idMhs,
                'sesi_id'       => $idSesi,
                'matakuliah_id' => $idMatkul,
                'status_input' =>'0',
            ]);
        return $res;
    }

    //Antisipasi input matkul yang sama
    // public static function getInputMahasiswaKK($idMatkul,$idMhs)
    // {
    //     $res =  DB::table('mahasiswas_sesis_matakuliahs')->where(
    //             [
    //             'mahasiswa_id'  => $idMhs,
    //             'matakuliah_id' => $idMatkul,
    //         ])->get();
    //     if($res->count()>0)
    //         return true;
    //     else
    //         return false;
    // }

    public static function setTerimaMatkul($idMhs,$idMatkul,$idSesi,$kp)
    {
        $date = Carbon::now();
        $date->setTimezone('Asia/Jakarta');
        DB::table('mahasiswas_sesis_matakuliahs')->where(
                [
                'mahasiswa_id'  => $idMhs,
                'sesi_id'       => $idSesi,
                'matakuliah_id' => $idMatkul,
                'kp' =>$kp,
            ])->update([
                'status_input'=>'1',
                'updated_at'=> $date->toDateTimeString(),
            ]);
    }
    public static function setTolakMatkul($idMhs,$idMatkul,$idSesi,$kp)
    {
        $date = Carbon::now();
        $date->setTimezone('Asia/Jakarta');
        DB::table('mahasiswas_sesis_matakuliahs')->where(
                [
                'mahasiswa_id'  => $idMhs,
                'sesi_id'       => $idSesi,
                'matakuliah_id' => $idMatkul,
                'kp' =>$kp,
            ])->update([
                'status_input'=>'0.5',
                'updated_at'=> $date->toDateTimeString(),
            ]);
    }
    public static function setCancelMatkul($idMhs,$idMatkul,$idSesi,$kp)
    {
        $date = Carbon::now();
        $date->setTimezone('Asia/Jakarta');
        DB::table('mahasiswas_sesis_matakuliahs')->where(
                [
                'mahasiswa_id'  => $idMhs,
                'sesi_id'       => $idSesi,
                'matakuliah_id' => $idMatkul,
                'kp' =>$kp,
            ])->update([
                'status_input'=>'0.75',
                'updated_at'=> $date->toDateTimeString(),
            ]);
    }

    //KasusKhusus Cek apakah boleh masuk atau tidak
    public static function getIsiRuanganMatkulKP($idMatkul,$kp)
    {
        $jumlahKapasitasRuang = MataKuliah::getJumlahKapasitas($idMatkul,$kp);

        $jumlahYangDiterima = MataKuliah::getJumlahDiterima($idMatkul,$kp);
        //Kapasitas Masih Cukup
        if($jumlahYangDiterima<$jumlahKapasitasRuang)
            return true;
        return false;
    }
    public static function getJumlahKapasitas($idMatkul,$kp)
    {
        $res = DB::table('jadwals_ruangans_matakuliahs')->join('ruangans','ruangans.id','=','jadwals_ruangans_matakuliahs.ruangan_id')->join('mata_kuliahs','mata_kuliahs.id','=','jadwals_ruangans_matakuliahs.matakuliah_id')->where([
            'jadwals_ruangans_matakuliahs.KP'=>$kp,
            'mata_kuliahs.id'=>$idMatkul,
            ])->min('ruangans.kapasitas');
        return $res;
    }
    public static function getJumlahDiterima($idMatkul,$kp)
    {
        $res = DB::table('mahasiswas_sesis_matakuliahs')->join('jadwals_ruangans_matakuliahs','jadwals_ruangans_matakuliahs.matakuliah_id','=','mahasiswas_sesis_matakuliahs.matakuliah_id')->where([
            'jadwals_ruangans_matakuliahs.KP'=>'mahasiswas_sesis_matakuliahs.KP',
            'jadwals_ruangans_matakuliahs.KP'=>$kp,
            'mahasiswas_sesis_matakuliahs.status_input'=>'1',
            'jadwals_ruangans_matakuliahs.matakuliah_id'=>$idMatkul,
            ])->count();
        return $res;
    }
}
