<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sesi extends Model
{
    protected $fillable=['nama_sesi','start_input','finish_input','isDone'];

    public function matakuliahs()
    {
    	return $this->belongsToMany('App\MataKuliah','mahasiswas_sesis_matakuliahs','sesi_id','matakuliah_id')->withPivot('status_input')->withTimestamps();
    }
    public function mahasiswas()
    {
    	return $this->belongsToMany('App\Mahasiswa','mahasiswas_sesis_matakuliahs','sesi_id','mahasiswa_id')->withPivot('status_input')->withTimestamps();
    }
}
