<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Dosen extends Model
{
    protected $fillable=['npk','nama','user_id'];

    public function user()
    {
    	return $this->belongsTo('App\User');
    }
    public function matakuliahs()
    {
    	return $this->belongsToMany('App\MataKuliah','dosens_matakuliahs','dosen_id','matakuliah_id')->withTimestamps();
    }
    public static function removeDosenMataKuliah($id,$idMatkul)
    {
    	$res = DB::table('dosens_matakuliahs')->where([
                ['dosen_id', '=', $id],
                ['matakuliah_id', '=', $idMatkul],
            ]);
    	return $res;
    }
    public static function deleteDosen($id,$idMatkul)
    {
        DB::table('dosens_matakuliahs')->where([
                ['dosen_id', '=', $id],
                ['matakuliah_id', '=', $idMatkul],
            ])->delete();
    }
    //Cek apakah dosen mengajar
    public static function cekDosen($id)
    {
        DB::table('dosens_matakuliahs')->where(['dosen_id','=',$id]);
    }
}
