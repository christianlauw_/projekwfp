<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JadwalKuliah extends Model
{
    protected $table = 'jadwal_kuliahs';
    protected $fillable=['hari','jam_mulai','jam_selesai'];

    public function ruangans()
    {
    	return $this->belongsToMany('App\Ruangan','jadwals_ruangans_matakuliahs','jadwalkuliah_id','ruangan_id')->withPivot('KP')->withTimestamps();
    }
    public function matakuliahs()
    {
    	return $this->belongsToMany('App\MataKuliah','jadwals_ruangans_matakuliahs','jadwalkuliah_id','matakuliah_id')->withPivot('KP')->withTimestamps();
    }
}
