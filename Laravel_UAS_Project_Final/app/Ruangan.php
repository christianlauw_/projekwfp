<?php

namespace App;
use DB;

use Illuminate\Database\Eloquent\Model;

class Ruangan extends Model
{
    protected $fillable=['nama_ruangan','kapasitas'];

    public function jadwalkuliahs()
    {
    	return $this->belongsToMany('App\JadwalKuliah','jadwals_ruangans_matakuliahs','ruangan_id','jadwalkuliah_id')->withPivot('KP')->withTimestamps();
    }
    public function matakuliahs()
    {
    	return $this->belongsToMany('App\MataKuliah','jadwals_ruangans_matakuliahs','ruangan_id','matakuliah_id')->withPivot('KP')->withTimestamps();
    }
    public static function cekRuanganTerpakai($idRuang)
    {
        //DB::table('jadwals_ruangans_matakuliahs')->where(['ruangan_id','=',$idRuang]);

    	$ruang = Ruangan::join('jadwals_ruangans_matakuliahs','jadwals_ruangans_matakuliahs.ruangan_id','=','ruangans.id')->get();
    	if($ruang->count()>0){
    		return true;
        }else{
    	   return false;
        }
    }
}
