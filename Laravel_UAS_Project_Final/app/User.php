<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password','role_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    public function role()
    {
        return $this->belongsTo('App\Role');
    }

    public function admin()
    {
        return $this->hasOne('App\Admin');
    }

    public function dosen()
    {
        return $this->hasOne('App\Dosen');
    }

    public function mahasiswa()
    {
        return $this->hasOne('App\Mahasiswa');
    }
    public function isAdmin()
    {
        return $this->role_id==1;
    }
    public function isMahasiswa()
    {
        return $this->role_id==3;
    }
    public function isDosen()
    {
        return $this->role_id==2;
    }
}
