<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;
class NewMahasiswaFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nrp' => 'unique:mahasiswas,nrp|required|max:9',
            'password' => 'required|min:5',
            'ipk'=>'required|numeric|between:0.00,4.00',
            'ips'=>'required|numeric|between:0.00,4.00',
            'sks'=>'numeric|required',
            'tahunMasuk'=>'numeric|required'
        ];
    }
}
