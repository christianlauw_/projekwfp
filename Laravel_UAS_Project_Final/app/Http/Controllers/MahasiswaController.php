<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Jurusan;
use App\Mahasiswa;
use App\User;

use App\Http\Requests\NewMahasiswaFormRequest;
use App\Http\Requests\MahasiswaUpdateFormRequest;

class MahasiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('isMahasiswa',Auth::user());
        return view('mahasiswas.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('isAdmin',Auth::user());
        $jur = Jurusan::get();
        $mhs = Mahasiswa::get();
        return view('mahasiswas.create',compact('jur','mhs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(NewMahasiswaFormRequest $request)
    {
        $email = $request->get('nrp').'mahasiswa.ac.id';
        $user = new User([
            'username'=>$request->get('nrp'),
            'password'=>bcrypt($request->get('password')),
            'email'=>$email,
            'role_id'=>3
            ]);
        $user->save();

        $mhs = new Mahasiswa([
            'nrp'=>$request->get('nrp'),
            'nama'=>$request->get('nama'),
            'ipk'=>$request->get('ipk'),
            'ips'=>$request->get('ips'),
            'sks'=>$request->get('sks'),
            'tahunMasuk'=>$request->get('tahunMasuk'),
            'isAsdos'=>0,
            'user_id'=>$user->id,
            'jurusan_id'=>$request->get('jurusan'),
            ]);
        $mhs->save();

        return redirect('mahasiswa/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect('mahasiswa');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('isAdmin',Auth::user());
        $jur = Jurusan::get();
        $mhs = Mahasiswa::find($id);
        return view('mahasiswas.edit',compact('jur','mhs','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MahasiswaUpdateFormRequest $request, $id)
    {
        $mhs = Mahasiswa::find($id);
        $mhs->jurusan_id=$request->get('jurusan');
        $mhs->sks=$request->get('sks');
        $mhs->tahunMasuk=$request->get('tahunMasuk');
        if($request->get('isAsdos')==true)
            $mhs->isAsdos = 1;
        else
            $mhs->isAsdos = 0;
        $mhs->save();
        return redirect('mahasiswa/create');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mhs = Mahasiswa::find($id);
        $user = User::find($mhs->user_id);
        if(!Mahasiswa::cekMahasiswa($mhs->id))
        {
            $mhs->delete();
            $user->delete();
        }
        return redirect('mahasiswa/create');
    }
}
