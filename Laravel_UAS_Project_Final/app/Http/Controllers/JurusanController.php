<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\NewJurusanFormRequest;
use App\Http\Requests\JurusanUpdateFormRequest;
use App\Jurusan;
use App\User;
use Auth;

class JurusanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('isAdmin', Auth::user());    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('isAdmin',Auth::user());
        $jurusan = Jurusan::get();
        return view('jurusan.create',compact('jurusan'));    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(NewJurusanFormRequest $request)
    {
        $jurusan = new Jurusan([
            'kode_jurusan'=>$request->get('kode_jurusan'),
            'nama_jurusan'=>$request->get('nama_jurusan'),
            ]);
        $jurusan->save();
        return redirect('jurusan/create');    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect('jurusan/create');    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('isAdmin',Auth::user());
        //$this->authorize('isAdmin');
        $jurusan = Jurusan::find($id);
        return view('jurusan.edit',compact('jurusan','id'));    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(JurusanUpdateFormRequest $request, $id)
    {
        $jurusan = Jurusan::find($id);
        $jurusan->nama_jurusan=$request->get('nama_jurusan');
        $jurusan->save();
        return redirect('jurusan/create');    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('isAdmin',Auth::user());
        //Jika Ruangan Belum terpakai baru boleh di delete
        
        if(!Jurusan::cekJurusanTerpakai($id))
        {
            $jurusan = Jurusan::find($id);
            $jurusan->delete();
        }
        return redirect('jurusan/create');    
    }
}
