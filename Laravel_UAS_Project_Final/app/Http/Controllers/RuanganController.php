<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\NewRuanganFormRequest;
use App\Http\Requests\RuanganUpdateFormRequest;
use App\Ruangan;
use Auth;
use App\User;
class RuanganController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('isAdmin', Auth::user());
        //return redirect('ruangan/create');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('isAdmin',Auth::user());
        //$ruang = Ruangan::all();
        $ruang = Ruangan::get();
        return view('ruangan.create',compact('ruang'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(NewRuanganFormRequest $request)
    {
        $ruang = new Ruangan([
            'nama_ruangan'=>$request->get('nama_ruangan'),
            'kapasitas'=>$request->get('kapasitas'),
            ]);
        $ruang->save();
        return redirect('ruangan/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect('ruangan/create');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('isAdmin',Auth::user());
        //$this->authorize('isAdmin');
        $ruang = Ruangan::find($id);
        return view('ruangan.edit',compact('ruang','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RuanganUpdateFormRequest $request, $id)
    {
        $ruang = Ruangan::find($id);
        // $ruang->nama_ruangan=$request->get('nama_ruangan');
        $ruang->kapasitas=$request->get('kapasitas');
        $ruang->save();
        return redirect('ruangan/create');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $this->authorize('isAdmin');
        $this->authorize('isAdmin',Auth::user());
        //Jika Ruangan Belum terpakai baru boleh di delete
        
        if(!Ruangan::cekRuanganTerpakai($id))
        {
            // return redirect('fpp/create');
            $ruang = Ruangan::find($id);
            $ruang->delete();
        }
        return redirect('ruangan/create');
    }
}
