<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jurusan;
use App\MataKuliah;
use Auth;
class JurusanMatakuliahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('isAdmin',Auth::user());
        $jurusan = Jurusan::get();
        $matkul = MataKuliah::get();
        
        return view('jurusanMatakuliah/create',compact('jurusan','matkul'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('isAdmin',Auth::user());
        $jur = Jurusan::find($request->get('jurusan'));
        $matkul = MataKuliah::find($request->get('matkul'));
        //Jika sudah di add tidak perlu add lagi
        $findJur = Jurusan::getJurusanMatakuliah($jur->id,$matkul->id);
        if($findJur->count()<=0)
            $matkul->jurusans()->attach($jur);
        return redirect('jurusanMatakuliah/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($idJur,$idMatkul)
    {
        $this->authorize('isAdmin',Auth::user());
        $jurusanMatkulToDelete = Jurusan::deleteMatkulFromJurusan($idJur, $idMatkul);
        if(!$jurusanMatkulToDelete)
           Jurusan::deleteJurusanMatkul($idJur,$idMatkul);
        return redirect('/jurusanMatakuliah/create');
    }
}
