<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use App\Sesi;
use App\Jurusan;
use App\MataKuliah;
use App\Mahasiswa;
use App\Http\Requests\FppFormRequest;
use Carbon\Carbon;
class FPPController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $sesi = Sesi::get();
        $fppRes = MataKuliah::getAllFpp();
        $jurusan = Jurusan::get();
        return view('fpp.index',compact('fppRes','jurusan','sesi'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('isMahasiswa',Auth::user());
        $fpp = DB::table('mahasiswas_sesis_matakuliahs')
                ->join('mahasiswas','mahasiswas_sesis_matakuliahs.mahasiswa_id','=','mahasiswas.id')
                ->join('sesis','mahasiswas_sesis_matakuliahs.sesi_id','=','sesis.id')
                ->join('mata_kuliahs','mahasiswas_sesis_matakuliahs.matakuliah_id','=','mata_kuliahs.id')->join('jurusans','jurusans.id','=','mahasiswas.jurusan_id')
                ->select('mata_kuliahs.kode_matkul as kode','mata_kuliahs.nama as nama','mata_kuliahs.sks as sks','mahasiswas_sesis_matakuliahs.status_input as status','sesis.id as sesi_id','mahasiswas.id as mhs_id','mahasiswas_sesis_matakuliahs.KP as kp','mata_kuliahs.id as id_matkul','mahasiswas.sks as mhs_sks')
                ->get();
        $sesi = Sesi::get();
        $sksDigunakan = Mahasiswa::sksTerpakai(Auth::user()->mahasiswa->id);
        $matkul = MataKuliah::getMatkulBuka(Auth::user()->mahasiswa->id);
        return view('fpp.create',compact('fpp','sesi','matkul','sksDigunakan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $matkul = MataKuliah::find($request->get('kodematkul'));
        $mhs = Mahasiswa::find($request['mhs']);
        $sesi = Sesi::find($request['sesi']);
        $sksUsed = Mahasiswa::sksTerpakai($request['mhs']);

        if($sksUsed+$matkul->sks<=$mhs->sks)
        {
            if(!MataKuliah::getInputMahasiswa($matkul->id,$request['mhs']))
            {
                $mhs->matakuliahs()->attach($matkul->id,['sesi_id'=>$sesi->id,'KP'=>$request['kp']]);
            }
        }
        return redirect('fpp/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($idmhs,$id_sesi,$idmatkul)
    {
        $matkul = MataKuliah::getInputMahasiswaToDelete($idmatkul,$idmhs,$id_sesi);
        if($matkul!=null)
            $matkul->delete();
        return redirect('fpp/create');
    }

    public function getKp($id)
    {
        $kp = MataKuliah::getKPMatkul($id);
        return response()->json([
            'kp'=>$kp
            ]);
        //return json_encode($kp);
    }
    public function getKapasitasSisa($idMatkul,$kp)
    {
        $jumKap = MataKuliah::getJumlahKapasitas($idMatkul,$kp);
        $isi = MataKuliah::getJumlahDiterima($idMatkul,$kp);
        return response()->json([
            'jumKap'=>$jumKap,
            'isi'=>$isi
            ]);
        //return json_encode($kp);
    }

    public function massUpdateFPP($id_Sesi) //Hanya untuk fpp 1 dan fpp 2
    {
        $this->authorize('isAdmin',Auth::user());
        //1. Ambil Semua Sesi
        $sesi = Sesi::find($id_Sesi); //Sesi yang mau diupdate

        //Ambil Sesi setelahnya
        $sesiNext = $id_Sesi+1;
        $sesi2 = Sesi::find($sesiNext);

        $getAllJadwal = MataKuliah::getMatkulBukaForFPP();
        $getAllFPP = MataKuliah::getAllFPP();

        //Ambil Jam sekarang
        $date = Carbon::now();
        $date->setTimezone('Asia/Jakarta');
        
        if($sesi->id!=3)
        {
            //FPP belum dijalankan
            if(!$sesi->isDone)
            {
                //Cek apakah sudah boleh start fpp
                //Kondisi start fpp 1 jika berada pada range time finish_input fpp 1 dan start input fpp 2 dst fpp 2 dan kasus khusus
                if($sesi->finish_input<$date->toDateTimeString() AND $sesi2->start_input>$date->toDateTimeString())
                {
                    //Untuk setiap jadwal dan kp yang dibuka cek ada pendaftar atau tidak
                    foreach($getAllJadwal as $item)
                    {
                        //Ambil id Matkul
                        $matkul  = MataKuliah::find($item->id_matkul);
                        
                        $jumlahIsiNow=MataKuliah::getIsiRuanganMatkulKP($matkul->id,$item->kp);
                        //Kalau Masih Cukup iterasi
                        if($jumlahIsiNow)
                        {
                            $arrTempUrus = array();
                            foreach($getAllFPP as $item2) //Ambil Semua input fpp 
                            {
                                //Cek apakah matakuliah dan kp terkait yang didaftar mhs sesuai dengan jadwal yang tersedia
                                if($item2->matakuliah_id==$matkul->id AND $item->kp==$item2->KP)
                                {
                                    if($sesi->id==$item2->Sesi) //sortir berdasar sesi terkait
                                    {
                                        if($item2->statInput==0) //jika pending urus
                                        {
                                            $arrTempUrus[] = array("idMhs"=>$item2->mhs_id,"id_matkul"=>$matkul->id,"kp"=>$item->kp,"sesi_id"=>$sesi->id);
                                        }
                                    }
                                }
                            }

                            $arrAsdos = array();
                            $arrSemesternya = array();
                            $arrAngkatanTua = array(); //Semester>10
                            $arrSisa = array();
                            //Cek apakah dia asdos , ambil matkul di angkatan semesternya atau angkatan tua
                            //dan kolom prioritas dihapus
                            foreach($arrTempUrus as $arrCekPriortas)
                            {
                                $date = Carbon::now();
                                $date->setTimezone('Asia/Jakarta');
                                $year = $date->year;
                                $month = $date->month;
                                $mhs = Mahasiswa::find($arrCekPriortas["idMhs"]);
                                
                                if($mhs->isAsdos)
                                    $arrAsdos[] = array("idMhs"=>$arrCekPriortas["idMhs"],"id_matkul"=>$arrCekPriortas["id_matkul"],"kp"=>$arrCekPriortas["kp"],"sesi_id"=>$arrCekPriortas["sesi_id"]);

                                //Asumsi
                                //Bulan 2-6 untuk Semester Ganjil //Input Bulan 7-1 untuk Semester Genap
                                elseif($month<=6 AND $month>=2)
                                {
                                    $semester = (date("Y")-($mhs->tahunMasuk))*2-1;
                                    if($matkul->semester==$semester)
                                        $arrSemesternya[]=array("idMhs"=>$arrCekPriortas["idMhs"],"id_matkul"=>$arrCekPriortas["id_matkul"],"kp"=>$arrCekPriortas["kp"],"sesi_id"=>$arrCekPriortas["sesi_id"]);
                                    elseif($semester>10)
                                    {
                                        $arrAngkatanTua[]=array("idMhs"=>$arrCekPriortas["idMhs"],"id_matkul"=>$arrCekPriortas["id_matkul"],"kp"=>$arrCekPriortas["kp"],"sesi_id"=>$arrCekPriortas["sesi_id"]);
                                    }
                                    else
                                    {
                                        $arrSisa[]=array("idMhs"=>$arrCekPriortas["idMhs"],"id_matkul"=>$arrCekPriortas["id_matkul"],"kp"=>$arrCekPriortas["kp"],"sesi_id"=>$arrCekPriortas["sesi_id"]);
                                    }
                                }
                                elseif($month>=7 OR $month<=1)
                                {
                                    $semester = (date("Y")-($mhs->tahunMasuk))*2;
                                    if($matkul->semester==$semester)
                                        $arrSemesternya[]=array("idMhs"=>$arrCekPriortas["idMhs"],"id_matkul"=>$arrCekPriortas["id_matkul"],"kp"=>$arrCekPriortas["kp"],"sesi_id"=>$arrCekPriortas["sesi_id"]);
                                    elseif($semester>10)
                                    {
                                        $arrAngkatanTua[]=array("idMhs"=>$arrCekPriortas["idMhs"],"id_matkul"=>$arrCekPriortas["id_matkul"],"kp"=>$arrCekPriortas["kp"],"sesi_id"=>$arrCekPriortas["sesi_id"]);
                                    }
                                    else
                                    {
                                        $arrSisa[]=array("idMhs"=>$arrCekPriortas["idMhs"],"id_matkul"=>$arrCekPriortas["id_matkul"],"kp"=>$arrCekPriortas["kp"],"sesi_id"=>$arrCekPriortas["sesi_id"]);
                                    }
                                }
                            }
                            
                            //Acak keberuntungan diterima
                            shuffle($arrAsdos);
                            shuffle($arrSemesternya);
                            shuffle($arrAngkatanTua);
                            shuffle($arrSisa);

                            //Proses Ubah Status
                            //Jika Sudah Penuh Tolak Semua
                            $jumlahIsiNow=MataKuliah::getIsiRuanganMatkulKP($matkul->id,$item->kp);
                            if(!$jumlahIsiNow)
                            {
                                foreach($arrAsdos as $ubahAsdos)
                                {
                                    //$mhs = Mahasiswa::find($ubahAsdos["idMhs"]);
                                    //ubah relasi mhs->matakuliahs dengan id matkul x dan ubah status=0.5
                                    // $mhs->matakuliahs()->updateExistingPivot($ubahAsdos["id_matkul"],["status_input"=>'0.5']);

                                    MataKuliah::setTolakMatkul($ubahAsdos["idMhs"],$ubahAsdos["id_matkul"],$ubahAsdos["sesi_id"],$ubahAsdos["kp"]);
                                }
                                foreach($arrSemesternya as $ubahSem)
                                {
                                    //$mhs = Mahasiswa::find($ubahSem["idMhs"]);
                                    // $mhs->matakuliahs()->updateExistingPivot($ubahSem["id_matkul"],["status_input"=>'0.5']);

                                    MataKuliah::setTolakMatkul($ubahSem["idMhs"],$ubahSem["id_matkul"],$ubahSem["sesi_id"],$ubahSem["kp"]);
                                }
                                foreach($arrAngkatanTua as $ubahAngTua)
                                {
                                    //$mhs = Mahasiswa::find($ubahSem["idMhs"]);
                                    // $mhs->matakuliahs()->updateExistingPivot($ubahSem["id_matkul"],["status_input"=>'0.5']);

                                    MataKuliah::setTolakMatkul($ubahAngTua["idMhs"],$ubahAngTua["id_matkul"],$ubahAngTua["sesi_id"],$ubahAngTua["kp"]);
                                }
                                foreach($arrSisa as $ubahSisa)
                                {
                                    //$mhs = Mahasiswa::find($ubahSisa["idMhs"]);
                                    //$mhs->matakuliahs()->updateExistingPivot($ubahSisa["id_matkul"],["status_input"=>'0.5']);

                                    MataKuliah::setTolakMatkul($ubahSisa["idMhs"],$ubahSisa["id_matkul"],$ubahSisa["sesi_id"],$ubahSisa["kp"]);
                                }
                            }
                            //Jika kapasitas masih cukup cek
                            else
                            {
                                foreach($arrAsdos as $ubahAsdos)
                                {
                                    $jumlahIsiNow=MataKuliah::getIsiRuanganMatkulKP($matkul->id,$item->kp);
                                    //Jika jumlah masih mencukupi terima semua asdos
                                    if($jumlahIsiNow)
                                    {
                                        //$mhs = Mahasiswa::find($ubahAsdos["idMhs"]);
                                        //Opsi 1 : resiko -> update semua yg punya fk mhs dan id matkul di relasi
                                        //$mhs->matakuliahs()->updateExistingPivot($ubahAsdos["id_matkul"],["status_input"=>'1']);
                                        //Opsi 2 : manual edit
                                        MataKuliah::setTerimaMatkul($ubahAsdos["idMhs"],$ubahAsdos["id_matkul"],$ubahAsdos["sesi_id"],$ubahAsdos["kp"]);
                                    }
                                    else
                                    {
                                        //$mhs = Mahasiswa::find($ubahAsdos["idMhs"]);
                                    //ubah relasi mhs->matakuliahs dengan id matkul x dan ubah status=0.5
                                        //$mhs->matakuliahs()->updateExistingPivot($ubahAsdos["id_matkul"],["status_input"=>'0.5']);
                                        
                                        MataKuliah::setTolakMatkul($ubahAsdos["idMhs"],$ubahAsdos["id_matkul"],$ubahAsdos["sesi_id"],$ubahAsdos["kp"]);
                                    }
                                }
                                foreach($arrSemesternya as $ubahSem)
                                {
                                    $jumlahIsiNow=MataKuliah::getIsiRuanganMatkulKP($matkul->id,$item->kp);
                                    if($jumlahIsiNow)
                                    {
                                        //$mhs = Mahasiswa::find($ubahSem["idMhs"]);
                                        //$mhs->matakuliahs()->updateExistingPivot($ubahSem["id_matkul"],["status_input"=>'1']);
                                        //Opsi 2 : manual edit
                                        
                                        MataKuliah::setTerimaMatkul($ubahSem["idMhs"],$ubahSem["id_matkul"],$ubahSem["sesi_id"],$ubahSem["kp"]);
                                    }
                                    else
                                    {
                                        //$mhs = Mahasiswa::find($ubahSem["idMhs"]);
                                    //ubah relasi mhs->matakuliahs dengan id matkul x dan ubah status=0.5
                                        //$mhs->matakuliahs()->updateExistingPivot($ubahSem["id_matkul"],["status_input"=>'0.5']);
                                         MataKuliah::setTolakMatkul($ubahSem["idMhs"],$ubahSem["id_matkul"],$ubahSem["sesi_id"],$ubahSem["kp"]);
                                    }
                                }
                                foreach($arrAngkatanTua as $ubahAngTua)
                                {
                                    $jumlahIsiNow=MataKuliah::getIsiRuanganMatkulKP($matkul->id,$item->kp);
                                    if($jumlahIsiNow)
                                    {
                                        //$mhs = Mahasiswa::find($ubahAngTua["idMhs"]);
                                        //$mhs->matakuliahs()->updateExistingPivot($ubahAngTua["id_matkul"],["status_input"=>'1']);
                                        //Opsi 2 : manual edit
                                         MataKuliah::setTerimaMatkul($ubahAngTua["idMhs"],$ubahAngTua["id_matkul"],$ubahAngTua["sesi_id"],$ubahAngTua["kp"]);
                                    }
                                    else
                                    {
                                        //$mhs = Mahasiswa::find($ubahAngTua["idMhs"]);
                                        //$mhs->matakuliahs()->updateExistingPivot($ubahAngTua["id_matkul"],["status_input"=>'0.5']);

                                        MataKuliah::setTolakMatkul($ubahAngTua["idMhs"],$ubahAngTua["id_matkul"],$ubahAngTua["sesi_id"],$ubahAngTua["kp"]);
                                    }
                                }
                                foreach($arrSisa as $ubahSisa)
                                {
                                    $jumlahIsiNow=MataKuliah::getIsiRuanganMatkulKP($matkul->id,$item->kp);
                                    if($jumlahIsiNow)
                                    {
                                        //$mhs = Mahasiswa::find($ubahSem["idMhs"]);
                                        //$mhs->matakuliahs()->updateExistingPivot($ubahSem["id_matkul"],["status_input"=>'1']);
                                        //Opsi 2 : manual edit
                                        MataKuliah::setTerimaMatkul($ubahSisa["idMhs"],$ubahSisa["id_matkul"],$ubahSisa["sesi_id"],$ubahSisa["kp"]);
                                    }
                                    else
                                    {
                                        //$mhs = Mahasiswa::find($ubahSisa["idMhs"]);
                                        //$mhs->matakuliahs()->updateExistingPivot($ubahSisa["id_matkul"],["status_input"=>'0.5']);

                                        MataKuliah::setTolakMatkul($ubahSisa["idMhs"],$ubahSisa["id_matkul"],$ubahSisa["sesi_id"],$ubahSisa["kp"]);
                                    }
                                }

                            }
                        }
                        
                    }
                    //Setelah dijalankan set sehingga ketika tombol dipencet tidak jalan 2x
                    $sesi->isDone=1;
                    $sesi->save();
                }
            }   
        }
        return redirect('/fpp');
    }
}
