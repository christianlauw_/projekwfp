<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Dosen;
use App\User;
use App\Http\Requests\NewDosenFormRequest;
use App\Http\Requests\DosenUpdateFormRequest;
class DosenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('isDosen',Auth::user());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('isAdmin',Auth::user());
        $dosen = Dosen::get();
        return view('dosens.create',compact('dosen'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(NewDosenFormRequest $request)
    {
        $email = $request->get('npk').'admin.ac.id';
        $user = new User([
            'username'=>$request->get('npk'),
            'password'=>bcrypt($request->get('pass')),
            'email'=>$email,
            'role_id'=>2
            ]);
        $user->save();

        $ad = new Dosen([
            'npk'=>$request->get('npk'),
            'nama'=>$request->get('nama'),
            'user_id'=>$user->id,
            ]);
        $ad->save();

        return redirect('dosen/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('isAdmin',Auth::user());
        $dos = Dosen::find($id);
        return view('dosens.edit',compact('dos','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DosenUpdateFormRequest $request, $id)
    {
        $dos = Dosen::find($id);
        $dos->nama=$request->get('nama');

        $dos->save();
        return redirect('dosen/create');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dos = Dosen::find($id);
        $user = User::find($dos->user_id);
        if(!Dosen::cekDosen($dos->id))
        {
            $dos->delete();
            $user->delete();
        }
        return redirect('dosen/create');
    }
}
