<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dosen;
use App\MataKuliah;
use Auth;
class DosenMataKuliahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('isAdmin',Auth::user());
        $dosen = Dosen::get();
        $matkul = MataKuliah::get();
        return view('dosenMatkul.create',compact('dosen','matkul'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('isAdmin',Auth::user());
        $dos = Dosen::find($request->get('dosen'));
        $matkul = MataKuliah::find($request->get('matkul'));
        $findDosen = Dosen::removeDosenMataKuliah($dos->id,$matkul->id);
        if($findDosen->count()<=0)
            $matkul->dosens()->attach($dos);
        return redirect('dosenMatkul/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,$idMatkul)
    {
         $this->authorize('isAdmin',Auth::user());
         $dosenMatkulToDelete = Dosen::removeDosenMataKuliah($id,$idMatkul);
         if($dosenMatkulToDelete->count()>0)
            Dosen::deleteDosen($id,$idMatkul);
         return redirect('/dosenMatkul/create');
    }
}
