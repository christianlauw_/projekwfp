<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Sesi;
class JadwalPerwalianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('isAdmin',Auth::user());
        $sesi = Sesi::get();
        return view('jadwalPerwalian.index',compact('sesi'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('isAdmin',Auth::user());
        $sesi = Sesi::find($id);
        return view('jadwalPerwalian.edit',compact('sesi','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $sesi = Sesi::find($id);
        $sesi->nama_sesi = $request->get('nama_sesi');
        $sesi->start_input = $request->get('start_input');
        $sesi->finish_input = $request->get('finish_input');
        if($request->get('isDone')==true)
            $sesi->isDone = 1;
        else
            $sesi->isDone = 0;
        $sesi->save();
        return redirect('/jadwalPerwalian');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
