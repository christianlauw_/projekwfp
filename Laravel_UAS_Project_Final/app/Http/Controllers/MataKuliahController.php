<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MataKuliah;
use App\JadwalKuliah;
use App\Ruangan;
use App\Jurusan;
use App\Day;
use Auth;
class MataKuliahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Tampilno Matkul dan Jumlahnya        
        $matkul = MataKuliah::get();
        return view('matakuliah.index',compact('matkul'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('isAdmin',Auth::user());
        $days = Day::get();
        return view('matakuliah.create',compact('days'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Belum bisa cek kalau sudah ada, Validation Request belum
        //insert matkul baru oleh admin
        $inputMatkul = new MataKuliah([
            'kode_matkul'=>$request->get('kode_matkul'),
            'semester'=>$request->get('semester'),
            'nama'=>$request->get('nama'),
            'sks'=>$request->get('sks'),
            'hari_ujian'=>$request->get('hari_ujian'),
            'minggu_ujian'=>$request->get('minggu_ujian'),
            'jam_ujian'=>$request->get('jam_ujian'),
            ]);
        $inputMatkul->save();        
        return redirect('matakuliah');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $showMatkul = MataKuliah::getMatkulById($id);
        $dosenAjar = MataKuliah::find($id);
        return view('matakuliah.show',compact('showMatkul','dosenAjar','id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('isAdmin',Auth::user());
        $matkul = MataKuliah::find($id);
        $days = Day::get();
        $ruangan = Ruangan::get();
        $jadwalKuliah = JadwalKuliah::get();
        return view('matakuliah.edit',compact('matkul','id','days','jadwalKuliah','ruangan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $matkul = MataKuliah::find($id);
        $matkul->kode_matkul = $request->get('kode_matkul');
        $matkul->nama = $request->get('nama');
        $matkul->sks = $request->get('sks');
        $matkul->hari_ujian = $request->get('hari_ujian');
        $matkul->minggu_ujian = $request->get('minggu_ujian');
        $matkul->jam_ujian = $request->get('jam_ujian');
        $matkul->semester = $request->get('semester');
        $matkul->save();
        return redirect('/matakuliah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('isAdmin',Auth::user());
        $cekMatkul = MataKuliah::getMatkulById($id);
        if($cekMatkul==null)
        {
            $matkul = MataKuliah::find($id);
            $matkul->delete();
        }
        return redirect('/matakuliah');
    }

}
