<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MataKuliah;
use App\JadwalKuliah;
use App\Ruangan;
use DB;
use Auth;
use Carbon\Carbon;
class JadwalKuliahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $matkul = MataKuliah::getAllJadwalKuliah();
        return view('jadwalMatkul.index',compact('matkul'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('isAdmin',Auth::user());
        $jadwalKuliah = JadwalKuliah::get();
        $ruangan = Ruangan::get();
        $mataKuliah = MataKuliah::get();
        return view('jadwalMatkul.create',compact('jadwalKuliah','ruangan','mataKuliah'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $matkul = MataKuliah::find($request->get('matkul'));
        if($matkul->sks>3) //Kalau Matkul yg ditambah jadwalnya sks > 3
        {
            $jadwal1 = JadwalKuliah::find($request->get('jadwal1'));
            $ruang1 = Ruangan::find($request->get('ruangan1'));
            $matkul->ruangans()->attach($ruang1->id,['jadwalkuliah_id'=>$jadwal1->id,'kp'=>$request->get('KP')]);
            
            $jadwal2 = JadwalKuliah::find($request->get('jadwal2'));
            $ruang2 = Ruangan::find($request->get('ruangan2'));
            $matkul->ruangans()->attach($ruang2->id,['jadwalkuliah_id'=>$jadwal2->id,'kp'=>$request->get('KP')]);
        }
        else
        {
            $jadwal1 = JadwalKuliah::find($request->get('jadwal1'));
            $ruang1 = Ruangan::find($request->get('ruangan1'));
            $matkul->ruangans()->attach($ruang1->id,['jadwalkuliah_id'=>$jadwal1->id,'kp'=>$request->get('KP')]);
        }
        return redirect('jadwalMatkul');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id,$kp)
    {
        $matkul = MataKuliah::getMataKuliahtoEdit($id,$kp);
        $jadwalKul = JadwalKuliah::get();
        $ruang = Ruangan::get();
        $mataKuliah = MataKuliah::get();

        return view('jadwalMatkul.edit',compact('matkul','mataKuliah','ruang','jadwalKul','id','kp'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id,$kp)
    {
        $findDelete = MataKuliah::deleteJadwalKuliah($id,$kp);
        $findDelete->delete();

        $matkul = MataKuliah::find($id);
        if($matkul->sks>3) //Kalau Matkul yg ditambah jadwalnya sks > 3
        {
            $jadwal1 = JadwalKuliah::find($request->get('jadwal1'));
            $ruang1 = Ruangan::find($request->get('ruangan1'));
            $matkul->ruangans()->attach($ruang1->id,['jadwalkuliah_id'=>$jadwal1->id,'kp'=>$request->get('KP')]);
            
            $jadwal2 = JadwalKuliah::find($request->get('jadwal2'));
            $ruang2 = Ruangan::find($request->get('ruangan2'));
            $matkul->ruangans()->attach($ruang2->id,['jadwalkuliah_id'=>$jadwal2->id,'kp'=>$request->get('KP')]);
        }
        else
        {
            $jadwal1 = JadwalKuliah::find($request->get('jadwal1'));
            $ruang1 = Ruangan::find($request->get('ruangan1'));
            $matkul->ruangans()->attach($ruang1->id,['jadwalkuliah_id'=>$jadwal1->id,'kp'=>$request->get('KP')]);
        }
        return redirect('jadwalMatkul');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
    public function getServerTime()
    {
        $date = Carbon::now();
        $date->setTimezone('Asia/Jakarta');
        return response()->json([
            'jam'=>$date->toDateTimeString(),
            'timezoneUsed'=>$date->tzName
            ]);
    }
}
