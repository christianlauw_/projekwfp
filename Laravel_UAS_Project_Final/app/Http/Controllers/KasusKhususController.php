<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mahasiswa;
use App\MataKuliah;
use App\Sesi;
use Auth;
class KasusKhususController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('isDosen',Auth::user());
        $mhs = Mahasiswa::get();
        return view('kasuskhusus.index',compact('mhs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return redirect('fpp/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    //Untuk Mahasiswa KK
    public function store(Request $request)
    {
        //Kasus Khusus yang dilakukan oleh mhs
        $mhs = Mahasiswa::find($request->get('mhs'));
        $matkul = MataKuliah::find($request->get('kodematkul')); //GetMatkulbyId
        
        //Jika dia belum pernah input maka baru boleh
        if(!MataKuliah::getInputMahasiswa($matkul->id,$mhs->id))
        {
            //Jika kapasitas masih bisa
            if(MataKuliah::getIsiRuanganMatkulKP($request->get('kodematkul'),$request->get('kp')))
            {
                $mhs->matakuliahs()->attach($matkul->id,['sesi_id'=>3,'status_input'=>'1','KP'=>$request->get('kp')]);
            }
            else
            {   
                $mhs->matakuliahs()->attach($matkul->id,['sesi_id'=>3,'status_input'=>'0','KP'=>$request->get('kp')]);
            }
        }
        return redirect('fpp/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->authorize('isDosen',Auth::user());
        $fppMhs = MataKuliah::getFPPMhs($id);
        $sesi = Sesi::get();
        return view('kasuskhusus.show',compact('fppMhs','sesi','id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Kasus khusus oleh dosen
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function cancelMatkul($idMhs,$idMatkul,$idSesi,$kp)
    {
        $this->authorize('isDosen',Auth::user());
        MataKuliah::setCancelMatkul($idMhs,$idMatkul,$idSesi,$kp);
        return redirect('/kasuskhusus'.'/'.$idMhs);
    }
}
