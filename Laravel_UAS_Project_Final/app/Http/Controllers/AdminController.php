<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Admin;
use App\User;
use App\Http\Requests\NewAdminFormRequest;
use App\Http\Requests\AdminUpdateFormRequest;
class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('isAdmin', Auth::user());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('isAdmin',Auth::user());
        $adm = Admin::get();
        return view('admins.create',compact('adm'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(NewAdminFormRequest $request)
    {
        $email = $request->get('npk').'admin.ac.id';
        $user = new User([
            'username'=>$request->get('npk'),
            'password'=>bcrypt($request->get('pass')),
            'email'=>$email,
            'role_id'=>1
            ]);
        $user->save();

        $ad = new Admin([
            'npk'=>$request->get('npk'),
            'nama'=>$request->get('nama'),
            'user_id'=>$user->id,
            ]);
        $ad->save();

        return redirect('admin/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('isAdmin',Auth::user());
        $adm = Admin::find($id);
        return view('admins.edit',compact('adm','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AdminUpdateFormRequest $request, $id)
    {
        $adm = Admin::find($id);
        $adm->nama = $request->get('nama');
        $adm->save();
        return redirect('/admin/create');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $adm = Admin::find($id);
        $user = User::find($adm->user_id);
        $adm->delete();
        $user->delete();
        return redirect('/admin/create');
    }

    //Sepertinya ini tidak terpakai
    // public function getFPP1ByAdmin()
    // {
    //     $this->authorize('isAdmin',User::class);
    //     $fpp1 = DB::table('mahasiswas_sesis_matakuliahs')
    //             ->join('mahasiswas','mahasiswas_sesis_matakuliahs.mahasiswa_id','=','mahasiswas.id')
    //             ->join('sesis','mahasiswas_sesis_matakuliahs.sesi_id','=','sesis.id')
    //             ->join('mata_kuliahs','mahasiswas_sesis_matakuliahs.matakuliah_id','=','mata_kuliahs.id')
    //             ->join('jadwals_ruangans_matakuliahs','mahasiswas_sesis_matakuliahs.matakuliah_id','=','matakuliah.id')
    //             ->select('mahasiswas.nrp', 'mahasiswas.nama','mata_kuliahs.nama','jadwals_ruangans_matakuliahs.KP')
    //             ->get();
    //     return view('mahasiswa.index',compact('fpp1'));
    // }

}
