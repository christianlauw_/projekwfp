<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prioritas extends Model
{
    protected $fillable=['jenis_prioritas'];

    public function mahasiswas()
    {
    	return $this->belongsToMany('App\Mahasiswa','mahasiswas_prioritas','prioritas_id','mahasiswa_id')->withTimestamps();
    }
}
