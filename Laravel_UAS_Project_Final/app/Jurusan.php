<?php 
 
namespace App; 
use DB;
 
use Illuminate\Database\Eloquent\Model; 
 
class Jurusan extends Model 
{ 
    protected $fillable=['kode_jurusan','nama_jurusan']; 
 
    public function mahasiswas() 
    { 
      return $this->belongsTo('App\Jurusan'); 
    } 
    public function matakuliahs() 
    { 
      return $this->belongsToMany('App\MataKuliah','jurusans_matakuliahs','jurusan_id','matakuliah_id')->withTimestamps(); 
    } 
    public static function cekJurusanTerpakai($idJurusan)
    {
        //DB::table('jurusans_matakuliahs')->where(['jurusan_id','=',$idJurusan]);

    	$jurusan = Jurusan::join('jurusans_matakuliahs','jurusans_matakuliahs.jurusan_id','=','jurusans.id')->get();
    	$jurusan2 = Jurusan::join('mahasiswas','mahasiswas.jurusan_id','=','jurusans.id')->get();
    	if($jurusan->count()>0 || $jurusan2->count()>0)
    		return true;
        return false;
    }
    public static function getJurusanMatakuliah($idJur,$idMatkul)
    {
        $res = DB::table('jurusans_matakuliahs')->where([
                ['jurusan_id', '=', $idJur],
                ['matakuliah_id', '=', $idMatkul],
            ]);
        return $res;
    }
    public static function deleteMatkulFromJurusan($idJur, $idMatkul)
    {
        $cekMahasiswa = DB::table('mahasiswas_sesis_matakuliahs')
            ->join('mahasiswas','mahasiswas.id','=','mahasiswas_sesis_matakuliahs.mahasiswa_id')
            ->join('mata_kuliahs','mata_kuliahs.id','=','mahasiswas_sesis_matakuliahs.matakuliah_id')
            ->join('jurusans_matakuliahs','jurusans_matakuliahs.matakuliah_id','=','mata_kuliahs.id')
            ->join('jurusans','jurusans.id','=','jurusans_matakuliahs.jurusan_id')
            ->where([
                ['jurusans.id', '=', $idJur],
                ['mata_kuliahs.id','=',$idMatkul],
                ['mahasiswas.jurusan_id','=',$idJur],
            ])->select('mahasiswas.id');

        if($cekMahasiswa->count()>0 )//|| $cekDiFpp->count()>0)
            return true;
        return false;
    }
    public static function deleteJurusanMatkul($idJur,$idMatkul)
    {
        DB::table('jurusans_matakuliahs')->where([
                ['jurusan_id', '=', $idJur],
                ['matakuliah_id', '=', $idMatkul],
            ])->delete();
    }
} 