<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/serverTime','JadwalKuliahController@getServerTime');

Route::resource('/matakuliah','MatakuliahController');

Route::resource('fpp','FPPController');
Route::delete('/fpp/{id_mhs}/{sesi_id}/{id_matkul}','FPPController@destroy');
Route::get('/fpp/ajax/{id}','FPPController@getKp');
Route::get('/fpp/data/massUpdates/{id}','FPPController@massUpdateFPP');
Route::get('/fpp/ajax/{idMatkul}/{kp}','FPPController@getKapasitasSisa');

Route::resource('/jadwalMatkul','JadwalKuliahController');
Route::get('/jadwalMatkul/{id}/edit/{kp}','JadwalKuliahController@edit');
Route::patch('/jadwalMatkul/{id}/{kp}','JadwalKuliahController@update');

Route::resource('/jadwalUjian','JadwalUjianController');

Route::resource('/profil','ProfilController');
Route::resource('/mahasiswa','MahasiswaController');
Route::resource('/dosen','DosenController');
Route::resource('/admin','AdminController');
Route::resource('/jurusan','JurusanController');

Route::get('/jadwalPerwalian','JadwalPerwalianController@index');
Route::get('/jadwalPerwalian/{id}/edit','JadwalPerwalianController@edit');
Route::patch('/jadwalPerwalian/{id}','JadwalPerwalianController@update');

Route::get('/dosenMatkul/create','DosenMataKuliahController@create');
Route::post('/dosenMatkul','DosenMataKuliahController@store');
Route::delete('/dosenMatkul/{id}/{idMatkul}','DosenMataKuliahController@destroy');

Route::get('/kasuskhusus','KasusKhususController@index');
Route::post('/kasuskhusus','KasusKhususController@store');
Route::get('/kasuskhusus/{idMhs}','KasusKhususController@show');
Route::get('/kasuskhusus/{idMhs}/{idMatkul}/{idSesi}/{kp}','KasusKhususController@cancelMatkul');

Route::resource('/ruangan','RuanganController');

Route::get('/jurusanMatakuliah/create','JurusanMatakuliahController@create');
Route::post('/jurusanMatakuliah','JurusanMatakuliahController@store');
Route::delete('/jurusanMatakuliah/{idJur}/{idMatkul}','JurusanMatakuliahController@destroy');