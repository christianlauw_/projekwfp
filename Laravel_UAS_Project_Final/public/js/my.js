$('a[name=\'edit\']').click(function(e){
	e.preventDefault();
	var url1 =  $(this).attr('href');
	var id_ke = $(this).attr('id_ke');

    if($(this).html()=="Edit")
    {
        $(this).html('Cancel');
        $.ajax({
                url: url1,
                success: function(result) 
                {                
                        $('div.coba-'+id_ke).html(result);
                    
                }
            });
    }
    else
    {
        $(this).html('Edit');
        $('div.coba-'+id_ke).html("");
    }
});

$('a#create_matkul').click(function(e){
    e.preventDefault();
   var url1 =  $(this).attr('href');

    if($(this).html()=="Create Mata Kuliah")
    {
        $(this).html('Cancel');
        $.ajax({
                url: url1,
                success: function(result) 
                {                
                        $('div#container_create_matkul').html(result);
                }
            });
    }
    else
    {
        $(this).html('Create Mata Kuliah');
        $('div#container_create_matkul').html("");
    } 
});

$('a[name=\'edit_admin\']').click(function(e){
    e.preventDefault();
    var url_admin =  $(this).attr('href');
    var id_admin = $(this).attr('id_admin');

    if($(this).html()=="Edit")
    {
        $(this).html('Cancel');
        $.ajax({
                url: url_admin,
                success: function(result) 
                {                
                    $('div.admin-'+id_admin).html(result);
                }
            });
    }
    else
    {
        $(this).html('Edit');
        $('div.admin-'+id_admin).html("");
    }
});

$('a[name=\'edit_dosen\']').click(function(e){
    e.preventDefault();
    var url_dosen =  $(this).attr('href');
    var id_dosen = $(this).attr('id_dosen');

    if($(this).html()=="Edit")
    {
        $(this).html('Cancel');
        $.ajax({
                url: url_dosen,
                success: function(result) 
                {                
                    $('div.dosen-'+id_dosen).html(result);
                }
            });
    }
    else
    {
        $(this).html('Edit');
        $('div.dosen-'+id_dosen).html("");
    }
});

$('a[name=\'edit_ruangan\']').click(function(e){
    e.preventDefault();
    var url_ruangan =  $(this).attr('href');
    var id_ruangan = $(this).attr('id_ruangan');

    if($(this).html()=="Edit")
    {
        $(this).html('Cancel');
        $.ajax({
                url: url_ruangan,
                success: function(result) 
                {                
                    $('div.ruang-'+id_ruangan).html(result);
                }
            });
    }
    else
    {
        $(this).html('Edit');
        $('div.ruang-'+id_ruangan).html("");
    }
});

//jurusan
$('a[name=\'edit_jurusan\']').click(function(e){
    e.preventDefault();
    var url_jurusan =  $(this).attr('href');
    var id_jurusan = $(this).attr('id_jurusan');

    if($(this).html()=="Edit")
    {
        $(this).html('Cancel');
        $.ajax({
                url: url_jurusan,
                success: function(result) 
                {                
                    $('div.jurusan-'+id_jurusan).html(result);
                }
            });
    }
    else
    {
        $(this).html('Edit');
        $('div.jurusan-'+id_jurusan).html("");
    }
});

$('a[name=\'edit_mhs\']').click(function(e){
    e.preventDefault();
    var url_mhs =  $(this).attr('href');
    var id_mhs = $(this).attr('id_mhs');

    if($(this).html()=="Edit")
    {
        $(this).html('Cancel');
        $.ajax({
                url: url_mhs,
                success: function(result) 
                {                
                    $('div.mhs-'+id_mhs).html(result);
                }
            });
    }
    else
    {
        $(this).html('Edit');
        $('div.mhs-'+id_mhs).html("");
    }
});

$('button#btn-search-status').click(function(){
    $('tr').each(function(){
        $(this).show();
    });
    if($('select#select-status').val()=="pending"){
        $('.stats-pending').show();
        $('tr[nama=\'status\']').each(function () 
        {
            
            if ($(this).find('.stats-ditolak').text()=='Ditolak' || 
                $(this).find('.stats-dicancel').text()=='Dicancel' ||
                $(this).find('.stats-diterima').text()=='Diterima') 
            {
                $(this).hide();
            }
        });
    }
    else if($('select#select-status').val()=="ditolak"){
        $('.stats-ditolak').show();
        $('tr[nama=\'status\']').each(function () 
        {
            
            if ($(this).find('.stats-pending').text()=='Pending' || 
                $(this).find('.stats-dicancel').text()=='Dicancel' ||
                $(this).find('.stats-diterima').text()=='Diterima') 
            {
                $(this).hide();
            }
        });
    }
    else if($('select#select-status').val()=="dicancel"){
        $('.stats-dicancel').show();
        $('tr[nama=\'status\']').each(function () 
        {
            
            if ($(this).find('.stats-pending').text()=='Pending' || 
                $(this).find('.stats-ditolak').text()=='Ditolak' ||
                $(this).find('.stats-diterima').text()=='Diterima') 
            {
                $(this).hide();
            }
        });
    }
    else if($('select#select-status').val()=="diterima"){
        $('.stats-diterima').show();
        $('tr[nama=\'status\']').each(function () 
        {
            
            if ($(this).find('.stats-pending').text()=='Pending' || 
                $(this).find('.stats-ditolak').text()=='Ditolak' ||
                $(this).find('.stats-dicancel').text()=='Dicancel') 
            {
                $(this).hide();
            }
        });
    }

});

//jadwal Matkul create tampilkan jadwal1 jadwal2 tergantung jumlah sks dari matkul tersebut
$('select[name=\'matkul\']').change(function(){
    //ganti sks
    $('input#sks').val($('option[name=\'tampung_sks-'+$(this).val()+'\']').attr('sks'));

    var sksMatkul = $('input#sks').val();

    if(sksMatkul > 3)
    {
        $('div#jadwalke2').show();   
    }
    else
    {
        $('div#jadwalke2').hide();   
    }
});